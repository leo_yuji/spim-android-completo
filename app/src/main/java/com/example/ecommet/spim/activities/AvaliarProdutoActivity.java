package com.example.ecommet.spim.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.adapter.CaracteristicasAdapterAvaliar;
import com.example.ecommet.spim.adapter.ImagensAdapter;
import com.example.ecommet.spim.fragments.ListaProdutosAvaliarFragment;
import com.example.ecommet.spim.fragments.MainFragment;
import com.example.ecommet.spim.modelo.Caracteristica;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Imagem;
import com.example.ecommet.spim.modelo.ListasAvaliar;
import com.example.ecommet.spim.modelo.Produto;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.ProdutoService;
import com.example.ecommet.spim.webservice.UsuarioService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ecommet on 25/05/2017.
 */

public class AvaliarProdutoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Produto produto;
    private Colaborador colaborador;
    private CaracteristicasAdapterAvaliar adapterCaracteristicas;
    private TextView txtNomeProduto;
    private Button btFinalizarAvaliacao;
    private ListView listCaracteristicas;
    private List<Imagem> listImagens, imagensNaoAvaliadas;
    private List<Produto> listProdutos;
    private List<Caracteristica> caracteristicas, caracsNaoAvaliadas;
    ImageView leftArrowButton, rightArrowButton;
    private AdapterViewFlipper viewFlipper;
    private boolean isSlideshowOn = false;
    private Imagem imagem;
    private AdapterViewFlipper imagensFlipper;
    private LinearLayout layoutImagensAvaliar;
    private TextView textNomeProduto;
    private View separadorImagem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliar_produto);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_toolbar));
        toolbar.setTitle("Avaliar produtos");
        listProdutos = new ArrayList<>();

        if (getIntent().hasExtra("produto")) {
            produto = (Produto) getIntent().getSerializableExtra("produto");
            listProdutos.add(produto);


        }


        if (getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getIntent().getSerializableExtra("colaborador");

        }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        txtNomeProduto = (TextView) findViewById(R.id.txtNomeDoProduto);
        txtNomeProduto.setText(produto.getNome());
        btFinalizarAvaliacao = (Button) findViewById(R.id.btFinalizar);
        listCaracteristicas = (ListView) findViewById(R.id.list_caracteristicas);
        viewFlipper = (AdapterViewFlipper) findViewById(R.id.image_view_flipper);
        leftArrowButton = (ImageView) findViewById(R.id.left_button);
        rightArrowButton = (ImageView) findViewById(R.id.right_button);


        caracteristicas = produto.getCaracteristicas();
        populaListaCaracteristicas();

        listImagens = produto.getImagens();
        layoutImagensAvaliar = (LinearLayout) findViewById(R.id.layoutImagensAvaliar);
        textNomeProduto = (TextView) findViewById(R.id.textNomeProduto);
        separadorImagem = findViewById(R.id.separadorImagem);
        imagensFlipper = (AdapterViewFlipper) findViewById(R.id.image_view_flipper);
        imagensNaoAvaliadas = new ArrayList<>();
        for (Imagem imagemNaoAvaliada : listImagens) {
            if (imagemNaoAvaliada.isAvaliada() == null) {
                imagensNaoAvaliadas.add(imagemNaoAvaliada);
            }

        }
        ImagensAdapter imagensAdapter = new ImagensAdapter(getApplicationContext(), imagensNaoAvaliadas);
        imagensFlipper.setAdapter(imagensAdapter);
        if (listImagens.size() != 0) {
            imagem = listImagens.get(viewFlipper.getDisplayedChild());
            leftArrowButton.setVisibility(View.VISIBLE);
            rightArrowButton.setVisibility(View.VISIBLE);
        } else {
            layoutImagensAvaliar.setVisibility(View.GONE);
            separadorImagem.setVisibility(View.GONE);
//            leftArrowButton.setVisibility(View.GONE);
//            rightArrowButton.setVisibility(View.GONE);
        }


        leftArrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSlideshowOn) {
                    viewFlipper.showPrevious();

                }


            }
        });


        rightArrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                viewFlipper.showNext();


            }
        });


        btFinalizarAvaliacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = 0;
                for (Caracteristica carac : produto.getCaracteristicas()) {
                    if (carac.isAvaliada() != null && carac.isAvaliada() == true) {
                        i++;
                    }
                }

                Log.e("TOTAL CARACS TRUE", i+"");

                int j = 0;
                for (Imagem img : produto.getImagens()) {
                    if (img.isAvaliada() != null && img.isAvaliada() == true) {
                        j++;
                    }
                }
                new avaliarCaracteristicas().execute();
            }
        });

    }


    private void populaListaCaracteristicas() {
        caracsNaoAvaliadas = new ArrayList<>();
        for (Caracteristica caracteristica : caracteristicas) {
            if (caracteristica.isAvaliada() == null) {
                caracsNaoAvaliadas.add(caracteristica);
            }
        }
        adapterCaracteristicas = new CaracteristicasAdapterAvaliar(this, caracsNaoAvaliadas, listCaracteristicas);
        listCaracteristicas.setAdapter(adapterCaracteristicas);


    }


    private class avaliarCaracteristicas extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {
            try {
                ListasAvaliar listas = new ListasAvaliar();
                listas.setCaracteristicas(caracsNaoAvaliadas);
                listas.setImagens(imagensNaoAvaliadas);
                int i = 0;
                for (Caracteristica carac : listas.getCaracteristicas()) {
                    if (carac.isAvaliada() != null && carac.isAvaliada() == true) {
                        i++;
                    }
                }
                Log.e("TASK TOTAL TRUE", i+"");
                Log.e("TASK TOTAL CARACS", listas.getCaracteristicas().size()+"");
                return ProdutoService.avaliarCaracteristicas(getApplicationContext(), produto.getId(), listas);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {

            Intent returnIntent = new Intent(AvaliarProdutoActivity.this, ListaProdutosAvaliarFragment.class);
            returnIntent.putExtra("produtos", listProdutos.size());

            setResult(RESULT_OK);
            finish();
        }

    }

}




