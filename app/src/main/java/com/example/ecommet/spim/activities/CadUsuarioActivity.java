package com.example.ecommet.spim.activities;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.utils.CustomDialog;
import com.example.ecommet.spim.utils.HiddenPassTransformationMethod;
import com.example.ecommet.spim.webservice.UsuarioService;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import static com.example.ecommet.spim.utils.Notifica.toastCurto;

public class CadUsuarioActivity extends AppCompatActivity implements DialogInterface.OnClickListener {
    private Colaborador colaborador;
    private EditText editNomeUsuario, editSobrenomeUsuario, editCPFUsuario, editTelefoneUsuario, editCelularUsuario, editEmailUsuario, editSenhaUsuario, editConfirmarSenhaUsuario;
    private ImageView imagePerfilUsuario;
    private final int GALERIA = 1;
    private String caminhoArquivo = "";
    private File fileImagem;
    Bitmap bmpFoto = null;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 999;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);


        if (Build.VERSION.SDK_INT >= 23) {
            checaPermissao();
        }

        // FOTO PERFIL
        imagePerfilUsuario = (ImageView) findViewById(R.id.image_perfil);

        // EDIT NOME
        editNomeUsuario = (EditText) findViewById(R.id.edit_nome);

        // EDIT SOBRENOME
        editSobrenomeUsuario = (EditText) findViewById(R.id.edit_sobrenome);

        // EDIT CPF
        editCPFUsuario = (EditText) findViewById(R.id.edit_cpf);

        // EDIT TELEFONE
        editTelefoneUsuario = (EditText) findViewById(R.id.edit_tell);

        // EDIT CELULAR
        editCelularUsuario = (EditText) findViewById(R.id.edit_cell);


        // EDIT EMAIl
        editEmailUsuario = (EditText) findViewById(R.id.edit_email);

        // EDIT SENHA
        editSenhaUsuario = (EditText) findViewById(R.id.edit_senha);
        editSenhaUsuario.setTransformationMethod(new HiddenPassTransformationMethod());
        // EDIT COMFIRMAR SENHA
        editConfirmarSenhaUsuario = (EditText) findViewById(R.id.edit_confirmarSenha);
        editConfirmarSenhaUsuario.setTransformationMethod(new HiddenPassTransformationMethod());

        PatternedTextWatcher patternCelular = new PatternedTextWatcher("(##)#####-####");

        editTelefoneUsuario.addTextChangedListener(new PatternedTextWatcher("(##)####-####"));
        editCelularUsuario.addTextChangedListener(patternCelular);
        editCPFUsuario.addTextChangedListener(new PatternedTextWatcher("###.###.###-##"));


        imagePerfilUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chamarGaleria(v);
            }
        });
    }

    public void chamarGaleria(View view) {
        // DIALOG CAMGALERIA
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.origem_imagem);
        String[] opcoes = {getString(R.string.galeria), getString(R.string.removerFoto)};
        builder.setItems(opcoes, this);
        AlertDialog dialogCamGaleria = builder.create();
        dialogCamGaleria.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent intent;
        switch (which) {
            case 0:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALERIA);
                break;
            case 1:

                imagePerfilUsuario.invalidate();
                imagePerfilUsuario.setImageResource(R.drawable.background_logo_activitycadastrar);
                caminhoArquivo = "";
                toastCurto(getApplication(), "Imagem removida!");

                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case GALERIA:
                    Uri uri = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                    int column_index;

                    if (cursor != null) {
                        column_index = cursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToFirst();
                        caminhoArquivo = cursor.getString(column_index);
                        ;
                    }


                    cursor.close();
                    if (caminhoArquivo != null) {
                        if (!caminhoArquivo.isEmpty()) {


                            try {
                                bmpFoto = null;
                                bmpFoto = rotateImageIfRequired(caminhoArquivo);
                                if (bmpFoto != null) {
                                    imagePerfilUsuario.setImageBitmap(bmpFoto);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(this, "Imagem inválida, selecione outra", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Imagem inválida, selecione outra", Toast.LENGTH_SHORT).show();
                    }

                    break;


                default:

                    break;

            }


        }
    }

    public  Bitmap rotateImageIfRequired(String imagePath) {
        int degrees = 0;

        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degrees = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    degrees = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    degrees = 270;
                    break;
            }
        } catch (IOException e) {
            Log.e("ImageError", "Error in reading Exif data of " + imagePath, e);
        }

        BitmapFactory.Options decodeBounds = new BitmapFactory.Options();
        decodeBounds.inJustDecodeBounds = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, decodeBounds);
        int numPixels = decodeBounds.outWidth * decodeBounds.outHeight;
        int maxPixels = 2048 * 1536; // requires 12 MB heap

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = (numPixels > maxPixels) ? 2 : 1;

        bitmap = BitmapFactory.decodeFile(imagePath, options);

        if (bitmap == null) {
            return null;
        }

        Matrix matrix = new Matrix();
        matrix.setRotate(degrees);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        return bitmap;
    }


    public void cadastrarColaboradorClick(View v) throws ParseException, IOException {
        if (editNomeUsuario.getText().toString().isEmpty() || editNomeUsuario.getText().length() < 2) {
            toastCurto(this, getString(R.string.valida_nome_usuario));
            editNomeUsuario.requestFocus();
        } else if
                (editSobrenomeUsuario.getEditableText().toString().isEmpty() || editSobrenomeUsuario.getText().length() < 2) {
            toastCurto(this, getString(R.string.valida_sobrenome_usuario));
            editSobrenomeUsuario.requestFocus();
        } else if (editCPFUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_cpf));
            editCPFUsuario.requestFocus();
        } else if (editTelefoneUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_telefone));
            editTelefoneUsuario.requestFocus();
        } else if (editCelularUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_celular));
            editCelularUsuario.requestFocus();
        } else if (editEmailUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_email));
            editEmailUsuario.requestFocus();
        } else if (editSenhaUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_senha_usuario));
            editSenhaUsuario.requestFocus();
        } else if (editConfirmarSenhaUsuario.getEditableText().toString().isEmpty()) {
            toastCurto(this, getString(R.string.valida_confirmar_senha_usuario));
            editConfirmarSenhaUsuario.requestFocus();
        } else {

            colaborador = new Colaborador();


            colaborador.setEmail(editEmailUsuario.getEditableText().toString());
            colaborador.setNome(editNomeUsuario.getEditableText().toString());
            colaborador.setSobrenome(editSobrenomeUsuario.getEditableText().toString());
            colaborador.setCpf(editCPFUsuario.getEditableText().toString());
            colaborador.setTelefone(editTelefoneUsuario.getEditableText().toString());
            colaborador.setCelular(editCelularUsuario.getEditableText().toString());


            String imagemBase64 = "";

            if (!caminhoArquivo.toString().equals("")) {
                Bitmap bmpImagem = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(caminhoArquivo)));
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmpImagem.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] imagem = stream.toByteArray();

                imagemBase64 = Base64.encodeToString(imagem, Base64.DEFAULT);
            } else if (caminhoArquivo.toString().equals("")) {
                imagemBase64 = "";
            }

            if (!imagemBase64.toString().equals("")) {
                colaborador.setCaminhoImagem(imagemBase64);
            } else {
                colaborador.setCaminhoImagem("");
            }


            if (editConfirmarSenhaUsuario.getText().toString().trim().equals(editSenhaUsuario.getText().toString().trim())) {
                colaborador.setSenha(editSenhaUsuario.getEditableText().toString());
                new SaveUsuarioTask().execute();

            } else {
                toastCurto(getApplicationContext(), "Suas senhas não conferem!");
                editConfirmarSenhaUsuario.setFocusable(true);
                editConfirmarSenhaUsuario.setText("");
                editConfirmarSenhaUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);


            }


        }
    }


    private class SaveUsuarioTask extends AsyncTask<Void, Void, String> {
        private String erroStatus;
        private String colaboradorRetorno;
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customDialog = new CustomDialog(CadUsuarioActivity.this);
            customDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return colaboradorRetorno = UsuarioService.cadastrarUsuario(CadUsuarioActivity.this, colaborador);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                erroStatus = e.getMessage();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {

            customDialog.dismiss();

            if (colaboradorRetorno != null) {
                toastCurto(CadUsuarioActivity.this, getString(R.string.sucess_cadastro));
                finish();


            } else {
                if (erroStatus.equals("401")) {
                    toastCurto(CadUsuarioActivity.this, "E-mail já cadastrado");
                    editEmailUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                    editEmailUsuario.requestFocus();
                    editEmailUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                editEmailUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
                            }
                        }
                    });
                } else if (erroStatus.equals("406")) {
                    toastCurto(CadUsuarioActivity.this, "CPF já cadastrado");
                    editCPFUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                    editCPFUsuario.requestFocus();
                    editCPFUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                editCPFUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

                            }
                        }
                    });
                } else if (erroStatus.equals("500")) {
                    toastCurto(CadUsuarioActivity.this, getString(R.string.erro_salvar));

                } else if (erroStatus.equals("403")) {
                    toastCurto(CadUsuarioActivity.this, "CPF inválido");
                    editCPFUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                    editCPFUsuario.requestFocus();
                    editCPFUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (!hasFocus) {
                                editCPFUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

                            }
                        }
                    });
                } else {

                    toastCurto(CadUsuarioActivity.this, getString(R.string.erro_salvar));
                }


            }
        }
    }

    @TargetApi(23)
    private void checaPermissao() {
        int permissaoMemoriaExterna = checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissaoMemoriaExterna != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }
        int permissaoCamera = checkSelfPermission(android.Manifest.permission.CAMERA);
        if (permissaoCamera != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
