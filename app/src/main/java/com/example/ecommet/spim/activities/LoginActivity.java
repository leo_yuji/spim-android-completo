package com.example.ecommet.spim.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Usuario;
import com.example.ecommet.spim.utils.HiddenPassTransformationMethod;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.UsuarioService;

import static com.example.ecommet.spim.utils.Notifica.DialogMensagem;
import static com.example.ecommet.spim.utils.Notifica.toastCurto;

public class LoginActivity extends AppCompatActivity {
    private EditText emailUsuario, senhaUsuario, recuperarEmailUsuario;
    private String usuarioEmail, usuarioSenha, usuarioRecuperarEmail;
    private Usuario colaboradorRecuperarEmail;
    private Colaborador colaborador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        emailUsuario = (EditText) findViewById(R.id.editEmail);
        senhaUsuario = (EditText) findViewById(R.id.editSenha);
        senhaUsuario.setTransformationMethod(new HiddenPassTransformationMethod());
        senhaUsuario.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    autenticarClick(v);
                    return true;
                }
                return false;
            }
        });
    }


    public void autenticarClick(View view) {
        if (emailUsuario.getText().toString().trim().isEmpty()) {
            emailUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
            emailUsuario.setFocusable(true);
            toastCurto(this, getString(R.string.valida_login_usuario));

        } else if (senhaUsuario.getText().toString().trim().isEmpty()) {
            senhaUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
            senhaUsuario.setFocusable(true);
            toastCurto(this, getString(R.string.valida_senha_usuario));

        } else {
            Prefs.gravarLogin(emailUsuario.getText().toString(), senhaUsuario.getText().toString(), getApplicationContext());
            usuarioEmail = emailUsuario.getText().toString();
            usuarioSenha = senhaUsuario.getText().toString();
            new LogarUsuarioTask().execute();


        }
    }

    public void cadastrarActivityClick(View view) {
        Intent intent = new Intent(LoginActivity.this, CadUsuarioActivity.class);
        startActivity(intent);

    }

    public void esqueceuSenha(View view) {
        dialogRecuperarConta();
    }


    private void dialogRecuperarConta() {

        /*
            ALERTDIALOG RECUPERAR SENHA
         */

        AlertDialog dialogRecuperarConta;

        // Creating alert Dialog with one Button
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        //Métdo que queria um dialog que cadastra uma tarefa;
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(LoginActivity.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.dialog_recuperar_conta_usuario, null);
        builder.setView(mView);

        recuperarEmailUsuario = (EditText) mView.findViewById(R.id.txtEmail);


        builder.setPositiveButton("Comfirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (recuperarEmailUsuario.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplication(), "Insira um email para recuperação", Toast.LENGTH_SHORT).show();
                } else {

                    // Recuperar dados

                    usuarioRecuperarEmail = recuperarEmailUsuario.getText().toString();


                    colaboradorRecuperarEmail = new Usuario();
                    colaboradorRecuperarEmail.setEmail(usuarioRecuperarEmail);
                    new EnviarEmailTask().execute();

                    onResume();

                }
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });


        dialogRecuperarConta = builder.create();
        dialogRecuperarConta.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        emailUsuario.setText("");
        senhaUsuario.setText("");
    }

    private class EnviarEmailTask extends AsyncTask<Void, Void, String> {
        private String erroStatus = "";
        private int SPLASH_TIME_OUT = 1000;

        @Override
        protected String doInBackground(Void... params) {
            try {
                return UsuarioService.recuperarSenha(LoginActivity.this, colaboradorRecuperarEmail);
            } catch (Exception e) {
                Log.e("ERRO STATUS", e.getMessage());
                erroStatus = e.getMessage();
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null || erroStatus.equals("500")) {
                DialogMensagem(LoginActivity.this, "Atenção", "Tente novamente mais tarde");
            } else {

                new Handler().postDelayed(new Runnable() {
                    /*
                     * Exibindo splash com um timer.
                     */
                    @Override
                    public void run() {
                        DialogMensagem(LoginActivity.this, "Envio com sucesso",
                                "Verifique seu email e siga as instruções de recuperação.");
                    }
                }, SPLASH_TIME_OUT);

            }
        }
    }


    public class LogarUsuarioTask extends AsyncTask<Void, Void, Colaborador> {
        @Override
        protected Colaborador doInBackground(Void... params) {
            try {
                return colaborador = UsuarioService.logar(getApplicationContext(), usuarioEmail, usuarioSenha);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();

                return null;
            }
        }

        @Override
        protected void onPostExecute(Colaborador c) {
            if (colaborador == null) {
                DialogMensagem(LoginActivity.this, "Login", getString(R.string.login_invalido));
            } else {
                colaborador.setEmail(usuarioEmail);
                colaborador.setSenha(usuarioSenha);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                intent.putExtra("colaborador", colaborador);
                startActivity(intent);

            }
        }
    }


}
