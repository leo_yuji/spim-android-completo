package com.example.ecommet.spim.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.fragments.AlterarDadosPessoaisFragment;
import com.example.ecommet.spim.fragments.AlterarSenhaFragment;
import com.example.ecommet.spim.fragments.ListaProdutosAvaliarFragment;
import com.example.ecommet.spim.fragments.CarteiraFragment;
import com.example.ecommet.spim.fragments.MainFragment;
import com.example.ecommet.spim.fragments.ListaProdutosPreencherFragment;
import com.example.ecommet.spim.fragments.RetirarCreditosFragment;
import com.example.ecommet.spim.interfaces.RoundedImageView;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.utils.CustomDialog;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.UsuarioService;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;


/**
 * Created by Ecommet on 18/05/2017.
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DialogInterface.OnClickListener {
    private NavigationView navigationView;
    private TextView nameUsuario, rakingUsuario, creditoUsuario;
    private RoundedImageView imagePerfilUsuario;
    private ImageView imagemPerfilDialog;
    private Button btnImgAlterar, btnImgCancelar;
    private View viewHeader;
    private Colaborador colaborador = null;
    private Boolean exit = false;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 999;
    private Fragment fragment = null;
    private final int GALERIA = 1;
    private String caminhoArquivo = "";
    private File fileImagem;
    private boolean imgSelecionada = false;
    private String imagemBase64 = "";
    Bitmap bmpFoto = null;
    private AlertDialog dialogAlterarImagem;
    private Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_toolbar));
        setSupportActionBar(toolbar);
        checaPermissao();

        if (getIntent().hasExtra("colaborador")) {
            this.colaborador = (Colaborador) getIntent().getSerializableExtra("colaborador");

        }
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        fragment = new MainFragment();

        if (fragment != null) {
            ((MainFragment) fragment).colaborador = colaborador;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.replace(R.id.content_main, fragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        MenuItem itemAvaliar = navigationView.getMenu().findItem(R.id.nav_avaliarProdutos);
        if (itemAvaliar != null) {
            if (colaborador.getRanking() < 50) {
                itemAvaliar.setVisible(false);
            } else {
                itemAvaliar.setVisible(true);
            }
        }
        navigationView.setNavigationItemSelectedListener(this);


        viewHeader = navigationView.getHeaderView(0);

        nameUsuario = (TextView) viewHeader.findViewById(R.id.nav_header_textName);
        creditoUsuario = (TextView) viewHeader.findViewById(R.id.nav_header_textCreditosUser);
        rakingUsuario = (TextView) viewHeader.findViewById(R.id.nav_header_textRaking);

        imagePerfilUsuario = RoundedImageView.class.cast(viewHeader.findViewById(R.id.image_item));
        imagePerfilUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlterarImagem();
            }
        });

        displaySelectedScreen(R.id.fragment_main);


        new BuscarColaboradorTask().execute();

    }



    private void dialogAlterarImagem() {
        imgSelecionada = false;

        /*
            ALERTDIALOG RECUPERAR SENHA
         */

        // Creating alert Dialog with one Button
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        //Métdo que queria um dialog que cadastra uma tarefa;
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(MainActivity.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.dialog_alterar_imagem_perfil, null);
        builder.setView(mView);

        imagemPerfilDialog = (ImageView) mView.findViewById(R.id.imgPerfil);
        btnImgAlterar = (Button) mView.findViewById(R.id.btnImgEditar);
        btnImgCancelar = (Button) mView.findViewById(R.id.btnImgCancelar);
        String fotoPerfilString = Prefs.getEndereco(getApplication()) + colaborador.getCaminhoImagem();
        if (fotoPerfilString != null) {
            Picasso.with(getApplication()).load(fotoPerfilString).error(R.drawable.logo_branco).into(imagemPerfilDialog);
        }

        dialogAlterarImagem = builder.create();
        dialogAlterarImagem.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAlterarImagem.show();

        btnImgAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imgSelecionada) {
                    btnImgAlterar.setEnabled(false);
                    new AlterarImagemTask().execute();
                } else {
                    chamarGaleria(v);
                }
            }
        });

        btnImgCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAlterarImagem.dismiss();
            }
        });
    }

    public class BuscarColaboradorTask extends AsyncTask<Void, Void, Colaborador> {
        private Colaborador colaboradorLogado = null;

        @Override
        protected Colaborador doInBackground(Void... params) {
            try {
                colaboradorLogado = UsuarioService.buscarColaborador(getApplicationContext());
                return colaboradorLogado;
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(Colaborador c) {
            if (colaboradorLogado != null) {
                Intent intent = new Intent(MainActivity.this, MainFragment.class);
                intent.putExtra("colaborador", colaboradorLogado);
                setResult(RESULT_OK, intent);
                colaborador = colaboradorLogado;

                if (fragment instanceof MainFragment) {
                    MainFragment fragMain = (MainFragment) fragment;
                    fragMain.colaborador = colaborador;
                    fragMain.context = context;
                    fragMain.onResume();
                }

                onResume();
            }


        }

    }

    public class AlterarImagemTask extends AsyncTask<Void, Void, String> {
        private boolean imagemAlterada = false;
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customDialog = new CustomDialog(MainActivity.this);
            customDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                imagemAlterada = true;
                return UsuarioService.alterarImagem(getApplicationContext(), colaborador.getId(), imagemBase64);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            if (imagemAlterada) {
                new BuscarColaboradorTask().execute();

                dialogAlterarImagem.dismiss();
            } else {
                Toast.makeText(MainActivity.this, "Erro ao tentar alterar imagem", Toast.LENGTH_SHORT).show();
            }

            customDialog.dismiss();

        }

    }

    public void chamarGaleria(View view) {
        // DIALOG CAMGALERIA
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(R.string.origem_imagem);
        String[] opcoes = {getString(R.string.galeria)};
        builder.setItems(opcoes, this);
        android.app.AlertDialog dialogCamGaleria = builder.create();
        dialogCamGaleria.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent intent;
        switch (which) {
            case 0:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALERIA);
                break;
            default:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALERIA:
                    Uri uri = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                    int column_index;

                    if (cursor != null) {
                        column_index = cursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToFirst();
                        caminhoArquivo = cursor.getString(column_index);

                    }


                    cursor.close();
                    if (caminhoArquivo != null) {
                        if (!caminhoArquivo.isEmpty()) {
                            try {
                                bmpFoto = null;
                                bmpFoto = rotateImageIfRequired(caminhoArquivo);
                                if (bmpFoto != null) {
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    bmpFoto.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                    imagemBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                                    if (imagemPerfilDialog != null) {

                                        imagemPerfilDialog.setImageBitmap(bmpFoto);
                                    }


                                    imgSelecionada = true;
                                    btnImgAlterar.setText("Salvar");
                                } else {
                                    Toast.makeText(this, "Erro ao selecionar imagem", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }


                    break;

                default:
                    new BuscarColaboradorTask().execute();
                    if (fragment instanceof ListaProdutosAvaliarFragment) {
                        ListaProdutosAvaliarFragment frag = (ListaProdutosAvaliarFragment) fragment;


                        frag.onResume();

                        onResume();
                    }

                    if (fragment instanceof ListaProdutosPreencherFragment) {
                        ListaProdutosPreencherFragment frag = (ListaProdutosPreencherFragment) fragment;

                        frag.onResume();


                    }

                    break;


            }


        }
    }

    public  Bitmap rotateImageIfRequired(String imagePath) {
        int degrees = 0;

        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degrees = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    degrees = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    degrees = 270;
                    break;
            }
        } catch (IOException e) {
            Log.e("ImageError", "Error in reading Exif data of " + imagePath, e);
        }

        BitmapFactory.Options decodeBounds = new BitmapFactory.Options();
        decodeBounds.inJustDecodeBounds = true;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, decodeBounds);
        int numPixels = decodeBounds.outWidth * decodeBounds.outHeight;
        int maxPixels = 2048 * 1536; // requires 12 MB heap

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = (numPixels > maxPixels) ? 2 : 1;

        bitmap = BitmapFactory.decodeFile(imagePath, options);

        if (bitmap == null) {
            return null;
        }

        Matrix matrix = new Matrix();
        matrix.setRotate(degrees);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);

        return bitmap;
    }

    @Override
    protected void onResume() {
        MenuItem itemAvaliar = navigationView.getMenu().findItem(R.id.nav_avaliarProdutos);
        if (itemAvaliar != null) {
            if (colaborador.getRanking() < 50) {
                itemAvaliar.setVisible(false);
            } else {
                itemAvaliar.setVisible(true);
            }
        }
        MenuItem itemRetirarCreditos = navigationView.getMenu().findItem(R.id.nav_retirarCreditos);
        if (itemRetirarCreditos != null) {
            if (colaborador.getCredito().doubleValue() <= 0) {
                itemRetirarCreditos.setVisible(false);
            } else {
                itemRetirarCreditos.setVisible(true);
            }
        }

        nameUsuario.setText(colaborador.getNome());
        creditoUsuario.setText(" " + colaborador.getCredito() + " e-coins");
        rakingUsuario.setText(String.valueOf(Long.valueOf(colaborador.getRanking())));

        String fotoPerfilString = Prefs.getEndereco(getApplication()) + colaborador.getCaminhoImagem();
        if (fotoPerfilString != null) {
            Picasso.with(getApplication()).load(fotoPerfilString).error(R.drawable.logo_branco).into(imagePerfilUsuario);
        }


        super.onResume();
    }

    private void displaySelectedScreen(int id) {
        switch (id) {
            case R.id.nav_home:
                fragment = new MainFragment();
                break;
            case R.id.nav_editrarUsuario:

                fragment = new AlterarDadosPessoaisFragment();


                break;

            case R.id.nav_editarSenha:
                fragment = new AlterarSenhaFragment();

                break;


            case R.id.nav_carteira:

                fragment = new CarteiraFragment();


                break;


            case R.id.nav_retirarCreditos:

                fragment = new RetirarCreditosFragment();


                break;

            case R.id.nav_preencherProdutos:

                fragment = new ListaProdutosPreencherFragment();


                break;


            case R.id.nav_avaliarProdutos:

                fragment = new ListaProdutosAvaliarFragment();


                break;
            case R.id.nav_sair:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);

                Prefs.gravarToken("", new Date(), getApplicationContext());
                Prefs.setConectado(getApplicationContext(), false);
                Prefs.limparLogin(getApplicationContext());
                startActivity(intent);
                break;
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.replace(R.id.content_main, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        displaySelectedScreen(id);


        return true;
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            super.onBackPressed();
            return;
        }

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag("HOME");
            if (fragment != null) {
                if (fragment.isVisible()) {
                    this.exit = true;
                    Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_SHORT).show();
                }
            } else {
                fragment = MainFragment.class.newInstance();
                getFragmentManager().popBackStack();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment, "HOME").commit();

            }
        } catch (Exception e) {

        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit = false;
            }
        }, 1000);
    }


    @TargetApi(23)
    private void checaPermissao() {
        int permissaoMemoriaExterna = checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissaoMemoriaExterna != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }
        int permissaoCamera = checkSelfPermission(android.Manifest.permission.CAMERA);
        if (permissaoCamera != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                    REQUEST_CODE_ASK_PERMISSIONS);
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
