package com.example.ecommet.spim.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.fragments.ListaProdutosAvaliarFragment;
import com.example.ecommet.spim.modelo.Atributo;
import com.example.ecommet.spim.modelo.Caracteristica;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Imagem;
import com.example.ecommet.spim.modelo.ListasAvaliar;
import com.example.ecommet.spim.modelo.Produto;
import com.example.ecommet.spim.modelo.TipoAtributo;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.AtributoService;
import com.example.ecommet.spim.webservice.ProdutoService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Ecommet on 25/05/2017.
 */

public class PreencherProdutoActivity extends AppCompatActivity implements DialogInterface.OnClickListener {
    private Toolbar toolbar;
    private Produto produto;
    private Colaborador colaborador;
    private List<Produto> listProdutos;
    private TextView txtNomeProduto;
    private TextView[] textViewCaracteristicas;
    private Button btAdcImagemProduto;
    private ProgressBar progressProduto;
    private ViewFlipper viewFlipper;
    private List<Caracteristica> caracteristicas;
    private List<Imagem> listImagens;
    private LinearLayout layoutAtributosPrincipais;
    private URL urls = null;
    private File fileImagem;
    private String fotoArrayString;
    private final int GALERIA = 1;
    private boolean isSlideshowOn = false;
    private Bitmap bmpImagemProduto = null;
    private ImageView imageViewAdiconarImagem;
    private LinearLayout layoutAdicionarImagem;
    private String caminhoArquivo;
    private Button btnFinalizar;
    private HashMap<EditText, Atributo> editTextHashMap = new HashMap<>();
    private EditText txtAtributoSecundario, txtValorCaracteristica;
    private String imagemBase64;
    private ListasAvaliar listas = new ListasAvaliar();
    private LinearLayout layoutImagensPreencher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preencher_produto);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.background_toolbar));
        toolbar.setTitle("Preencher Produto");
        listProdutos = new ArrayList<>();
        if (getIntent().hasExtra("produto")) {
            produto = (Produto) getIntent().getSerializableExtra("produto");
            listProdutos.add(produto);
        }

        if (getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getIntent().getSerializableExtra("colaborador");

        }

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        layoutAtributosPrincipais = (LinearLayout) findViewById(R.id.layout_atributos_principais);
        txtNomeProduto = (TextView) findViewById(R.id.txtNomeDoProduto);
        txtNomeProduto.setText(produto.getNome());
        progressProduto = (ProgressBar) findViewById(R.id.progress_bar);
        progressProduto.setProgress(produto.getProgresso());
        textViewCaracteristicas = new TextView[6];
        textViewCaracteristicas[0] = (TextView) findViewById(R.id.txtCaracteristicas1);
        textViewCaracteristicas[1] = (TextView) findViewById(R.id.txtCaracteristicas2);
        textViewCaracteristicas[2] = (TextView) findViewById(R.id.txtCaracteristicas3);
        textViewCaracteristicas[3] = (TextView) findViewById(R.id.txtCaracteristicas4);
        textViewCaracteristicas[4] = (TextView) findViewById(R.id.txtCaracteristicas5);
        textViewCaracteristicas[5] = (TextView) findViewById(R.id.txtCaracteristicas6);
        imageViewAdiconarImagem = (ImageView) findViewById(R.id.image_view_adicionar_imagem);
        layoutAdicionarImagem = (LinearLayout) findViewById(R.id.layout_adicionar_imagem);
        txtAtributoSecundario = (EditText) findViewById(R.id.txtAtributoSecundario);
        txtValorCaracteristica = (EditText) findViewById(R.id.txtValorCaracteristica);
        btAdcImagemProduto = (Button) findViewById(R.id.bt_AdcImagemProduto);
        btnFinalizar = (Button) findViewById(R.id.btnFinalizarPreencher);

        btAdcImagemProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogChamarGaleria();
            }
        });

        viewFlipper = (ViewFlipper) findViewById(R.id.image_view_flipper);


        caracteristicas = produto.getCaracteristicas();

        for (int i = 0; i < caracteristicas.size(); i++) {
            if (i == 6) {
                break;
            }
            textViewCaracteristicas[i].setText(caracteristicas.get(i).getAtributo().getNome() +": " + caracteristicas.get(i).getValor());
        }

        layoutImagensPreencher = (LinearLayout) findViewById(R.id.layoutImagensPreencher);
        listImagens = produto.getImagens();

        if (listImagens.size() != 0) {

            for (Imagem imagem : listImagens) {
                fotoArrayString = Prefs.getEndereco(getApplication()) + imagem.getCaminho();


                try {
                    urls = new URL(fotoArrayString);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                try {
                    Bitmap image = BitmapFactory.decodeStream(urls.openConnection().getInputStream());
                    setFlipperImage(image);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else {
            layoutImagensPreencher.setVisibility(View.GONE);
        }

        ImageView leftArrowButton = (ImageView) findViewById(R.id.left_button);
        leftArrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSlideshowOn) {
                    viewFlipper.showPrevious();
                }
            }
        });

        ImageView rightArrowButton = (ImageView) findViewById(R.id.right_button);
        rightArrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                viewFlipper.showNext();
            }
        });

        new buscarAtributosPrincipais().execute();

    }

    private class buscarAtributosPrincipais extends AsyncTask<Void, Void, List<Atributo>> {
        private List<Atributo> atributosPrincipais = null;


        @Override
        protected List<Atributo> doInBackground(Void... params) {
            try {
                atributosPrincipais = AtributoService.listarAtributosPrincipais(getApplicationContext());
                return atributosPrincipais;
            } catch (Exception e) {
                Log.e("ERRO STATUS TASK", e.getMessage());
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(List<Atributo> atributos) {
            if (atributosPrincipais != null) {
                for (Atributo atributoPrincipal : atributosPrincipais) {
                    EditText textAtributoPrincipal = new EditText(getApplicationContext());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(dpToPx(10), dpToPx(10), dpToPx(10), 0);
                    textAtributoPrincipal.setLayoutParams(lp);
                    textAtributoPrincipal.setPaddingRelative(dpToPx(5), 0, 0, 0);
                    textAtributoPrincipal.setBackgroundResource(R.drawable.background_edittext_activity);
                    textAtributoPrincipal.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorBlack));
                    textAtributoPrincipal.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextDark));
                    textAtributoPrincipal.setHint(atributoPrincipal.getNome());
                    editTextHashMap.put(textAtributoPrincipal, atributoPrincipal);
                    layoutAtributosPrincipais.addView(textAtributoPrincipal);
                }

                List<Caracteristica> caracteristicas =  new ArrayList<>();
                List<Imagem> imagens = new ArrayList<>();
                btnFinalizar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (imagemBase64 != null) {
                            Imagem imagem = new Imagem();
                            imagem.setImagemAndroid(imagemBase64);
                            imagens.add(imagem);
                        }

                        for (int i = 0; i < editTextHashMap.size(); i++) {
                            EditText editText = (EditText) editTextHashMap.keySet().toArray()[i];
                            Atributo atributoPrincipal = editTextHashMap.get(editText);

                            if (!editText.getEditableText().toString().trim().equals("")) {
                                Caracteristica caracteristicaPrincipal = new Caracteristica();
                                caracteristicaPrincipal.setValor(editText.getEditableText().toString().trim());
                                caracteristicaPrincipal.setAtributo(atributoPrincipal);
                                caracteristicaPrincipal.setProduto(produto);
                                caracteristicas.add(caracteristicaPrincipal);
                            }

                        }

                        String nomeAtributoSecundario = txtAtributoSecundario.getEditableText().toString().trim();
                        String valorCaracteristica = txtValorCaracteristica.getEditableText().toString().trim();

                        if (!nomeAtributoSecundario.equals("") && !valorCaracteristica.equals("")) {
                            Caracteristica caracteristicaSecundaria = new Caracteristica();
                            caracteristicaSecundaria.setValor(valorCaracteristica);
                            caracteristicaSecundaria.setProduto(produto);
                            Atributo atributoSecundario = new Atributo();
                            atributoSecundario.setTipoAtributo(TipoAtributo.SECUNDARIO);
                            atributoSecundario.setNome(nomeAtributoSecundario);
                            caracteristicaSecundaria.setAtributo(atributoSecundario);
                            caracteristicas.add(caracteristicaSecundaria);
                        }

                        listas.setCaracteristicas(caracteristicas);
                        listas.setImagens(imagens);
                        new preencherProduto().execute();
                    }
                });
            }
        }

    }

    private class preencherProduto extends AsyncTask<Void, Void, String> {



        @Override
        protected String doInBackground(Void... params) {
            try {
                return ProdutoService.preencherProduto(getApplicationContext(), produto.getId(),listas);
            } catch (Exception e) {
                Log.e("ERRO STATUS", e.getMessage());
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String s) {
            Intent returnIntent = new Intent(PreencherProdutoActivity.this, ListaProdutosAvaliarFragment.class);
            returnIntent.putExtra("produtos", listProdutos.size());
            setResult(RESULT_OK);
            finish();
        }

    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void setFlipperImage(Bitmap res) {
        ImageView image = new ImageView(getApplicationContext());
        image.setImageBitmap(res);
        viewFlipper.addView(image);


    }

    private void dialogChamarGaleria(){
        // DIALOG CAMGALERIA
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.origem_imagem);
        String[] opcoes = {getString(R.string.galeria)};
        builder.setItems(opcoes, this);
        AlertDialog dialogCamGaleria = builder.create();
        dialogCamGaleria.show();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent intent;
        switch (which) {
            case 0:
                intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALERIA);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALERIA:
                    Uri uri = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                    int column_index;

                    if (cursor != null) {
                        column_index = cursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        cursor.moveToFirst();
                        caminhoArquivo = cursor.getString(column_index);
                    }


                    cursor.close();
                    if (caminhoArquivo != null) {
                        fileImagem = new File(caminhoArquivo);
                        try {
                            bmpImagemProduto = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(fileImagem));
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bmpImagemProduto.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream .toByteArray();
                            imagemBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

                            imageViewAdiconarImagem.setImageBitmap(bmpImagemProduto);
                            imageViewAdiconarImagem.setVisibility(View.VISIBLE);
                            LinearLayout.LayoutParams paramsImageView = new LinearLayout.LayoutParams(0, dpToPx(100), 0.4f);
                            imageViewAdiconarImagem.setLayoutParams(paramsImageView);

                            btAdcImagemProduto.setText("trocar imagem");
                            LinearLayout.LayoutParams paramsButton = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.6f);
                            paramsButton.gravity = Gravity.CENTER_VERTICAL;
                            btAdcImagemProduto.setLayoutParams(paramsButton);

                            layoutAdicionarImagem.setOrientation(LinearLayout.HORIZONTAL);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                    break;


                default:

                    break;

            }
            if (requestCode == 0) {
                this.produto = (Produto) getIntent().getSerializableExtra("produto");
            }

        }

        }


}
