package com.example.ecommet.spim.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.UsuarioService;

/**
 * Created by Ecommet on 14/06/2017.
 */

public class SplashScreenActivity extends AppCompatActivity {
    private Colaborador colaborador = null;
    private String usuarioEmail, usuarioSenha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!Prefs.getAcesso(getApplicationContext())[0].isEmpty() && !Prefs.getAcesso(getApplicationContext())[1].isEmpty()) {
                    usuarioEmail = Prefs.getAcesso(getApplicationContext())[0];
                    usuarioSenha = Prefs.getAcesso(getApplicationContext())[1];

                    new LogarUsuarioTask().execute();

                } else {
                    mostrarLogin();
                }

            }
        }, 2000);
    }

    private void mostrarLogin() {
        Intent intent = new Intent(SplashScreenActivity.this,
                LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public class LogarUsuarioTask extends AsyncTask<Void, Void, Colaborador> {
        @Override
        protected Colaborador doInBackground(Void... params) {
            try {
                return colaborador = UsuarioService.logar(getApplicationContext(), usuarioEmail, usuarioSenha);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();

                return null;
            }
        }

        @Override
        protected void onPostExecute(Colaborador c) {
            if (colaborador != null) {

                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);

                intent.putExtra("colaborador", colaborador);
                startActivity(intent);

            } else {
                mostrarLogin();
            }
        }
    }

}