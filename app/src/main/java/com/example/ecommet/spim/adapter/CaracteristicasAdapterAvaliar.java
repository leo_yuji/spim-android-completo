package com.example.ecommet.spim.adapter;

/**
 * Created by Ecommet on 19/04/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Caracteristica;
import com.example.ecommet.spim.modelo.Colaborador;

import java.util.List;


/**
 * Created by sn1022208 on 26/01/2017.
 */

public class CaracteristicasAdapterAvaliar extends ArrayAdapter<Caracteristica> {

    private TextView textCaracteristicas;
    private ImageButton buttonLike, buttonDislike;
    private Colaborador colaborador;
    private ListView listView;
    private Context context;
    private List<Caracteristica> caracteristicas;


    public CaracteristicasAdapterAvaliar(Context context, List<Caracteristica> caracteristicas,  ListView listView) {
        super(context, 0, caracteristicas);
        this.colaborador = colaborador;
        this.caracteristicas = caracteristicas;
        this.listView = listView;
        this.context = context;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ImageButton buttonLike, buttonDislike;

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_caracteristicas, parent, false);
        }


        textCaracteristicas = (TextView) view.findViewById(R.id.txtCaracteristicas);
        buttonLike = (ImageButton) view.findViewById(R.id.btLike);
        buttonDislike = (ImageButton) view.findViewById(R.id.btDislike);

        if (!caracteristicas.isEmpty()) {
            final Caracteristica caracteristica = caracteristicas.get(position);

            if (caracteristica.isAvaliada() == null) {
                buttonLike.setBackground(context.getDrawable(R.drawable.like_cinza));
                buttonDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));
            } else {
                if (caracteristica.isAvaliada() == true) {
                    buttonLike.setBackground(context.getDrawable(R.drawable.ic_like));
                    buttonDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));
                } else if (caracteristica.isAvaliada() == false) {
                    buttonDislike.setBackground(context.getDrawable(R.drawable.ic_dislike));
                    buttonLike.setBackground(context.getDrawable(R.drawable.like_cinza));
                }
            }
            buttonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    caracteristica.setAvaliada(true);
                    buttonLike.setBackgroundColor(context.getResources().getColor(R.color.circular_red));
                    buttonLike.setBackground(context.getDrawable(R.drawable.ic_like));
                    buttonDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));

                }
            });
            buttonDislike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    caracteristica.setAvaliada(false);
                    buttonDislike.setBackground(context.getDrawable(R.drawable.ic_dislike));
                    buttonLike.setBackground(context.getDrawable(R.drawable.like_cinza));

                }
            });

            textCaracteristicas.setText(caracteristica.getAtributo() + " " + caracteristica.getValor());
            this.listView.invalidateViews();
        }


        return view;
    }


}