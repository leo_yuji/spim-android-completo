package com.example.ecommet.spim.adapter;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.MovimentoCredito;
import com.example.ecommet.spim.modelo.TipMovement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ecommet on 31/03/2017.
 */

public class HistoricoTransacoesAdapter extends RecyclerView.Adapter<HistoricoTransacoesAdapter.MyViewHolder> {

    private FragmentActivity context;
    private List<MovimentoCredito> movimentoCreditoList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView dataTrancao, tipoTransferencia, valorTransferencia, quantidadeCreditos;
        public ImageView imageTipoTransacao;


        public MyViewHolder(View view) {
            super(view);
            dataTrancao = (TextView) view.findViewById(R.id.dataTransacao);
            tipoTransferencia = (TextView) view.findViewById(R.id.tipoTransferencia);
            valorTransferencia = (TextView) view.findViewById(R.id.valorTransferencia);
            quantidadeCreditos = (TextView) view.findViewById(R.id.quantidadeCoins);
            imageTipoTransacao = (ImageView) view.findViewById(R.id.imgTipoTransacao);

        }


    }


    public HistoricoTransacoesAdapter(FragmentActivity mContext, List<MovimentoCredito> mMovimentoCreditoList) {
        this.context = mContext;
        this.movimentoCreditoList = mMovimentoCreditoList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.historico_transacoes_card, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(HistoricoTransacoesAdapter.MyViewHolder holder, int position) {
        MovimentoCredito movimentoCredito = movimentoCreditoList.get(position);



    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        holder.dataTrancao.setText("Data: " + sdf.format(movimentoCredito.getDataAcao()));


        TipMovement tipoMovimento = movimentoCredito.getTipoMovimento();
        String tipoMovimentoString = "";

        switch (tipoMovimento) {
            case ENTRADA:
                tipoMovimentoString = "Entrada";
                break;
            case SAIDA:
                tipoMovimentoString = "Saida";
                break;

            default:
                break;
        }

        if (tipoMovimento != null) {
            holder.tipoTransferencia.setText(tipoMovimentoString);

            if (tipoMovimentoString.equals("Entrada")){
                holder.imageTipoTransacao.setImageResource(R.drawable.ic_entrada);
            } else {
                holder.imageTipoTransacao.setImageResource(R.drawable.saida);
            }
        } else {
        }



        holder.quantidadeCreditos.setText(String.valueOf(movimentoCredito.getEcoins()) + " e-coins");

        if (movimentoCredito.getValor() != null) {
            holder.valorTransferencia.setText("R$ " + String.valueOf(movimentoCredito.getValor()));
        } else {
            holder.valorTransferencia.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return movimentoCreditoList.size();
    }

    String strDateToShow(String dateToFormat){
        // format date to display
        SimpleDateFormat formatFrom, formatTo;
        formatFrom = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss zzz yyyy", Locale.ENGLISH);
        formatTo   = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        if(dateToFormat != null) {
            try {
                Date mDate = formatFrom.parse(dateToFormat);
                dateToFormat = formatTo.format(mDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(dateToFormat == null){
            dateToFormat = formatTo.format(new Date());
        }
        return dateToFormat;
    }
}
