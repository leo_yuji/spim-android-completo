package com.example.ecommet.spim.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Imagem;
import com.example.ecommet.spim.utils.Prefs;

import java.net.URL;
import java.util.List;

public class ImagensAdapter extends BaseAdapter {
    private Context context;
    private List<Imagem> imagens;
    private LayoutInflater inflter;

    public ImagensAdapter(Context applicationContext, List<Imagem> imagens) {
        this.context = applicationContext;
        this.imagens = imagens;
        inflter = (LayoutInflater.from(applicationContext));

    }

    @Override
    public int getCount() {
        return imagens.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.content_view_flipper, null);
        ImageView imagemProduto = (ImageView) view.findViewById(R.id.imagemProduto);
        final ImageButton btnLike = (ImageButton) view.findViewById(R.id.bt_imagemAvaliarLike);
        final ImageButton btnDislike = (ImageButton) view.findViewById(R.id.bt_imagemAvaliarDisklike);

        if (!imagens.isEmpty()) {

            final Imagem imagem = imagens.get(position);

            if (imagem.isAvaliada() == null) {
                btnLike.setBackground(context.getDrawable(R.drawable.like_cinza));
                btnDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));
            } else {
                if (imagem.isAvaliada() == true) {
                    btnLike.setBackground(context.getDrawable(R.drawable.ic_like));
                    btnDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));
                } else if (imagem.isAvaliada() == false) {
                    btnDislike.setBackground(context.getDrawable(R.drawable.ic_dislike));
                    btnLike.setBackground(context.getDrawable(R.drawable.like_cinza));
                }
            }

            String fotoArrayString = Prefs.getEndereco(context) + imagem.getCaminho();
                try {
                    URL url = new URL(fotoArrayString);
                    Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    imagemProduto.setImageBitmap(image);

                    btnLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imagem.setAvaliada(true);
                            btnLike.setBackground(context.getDrawable(R.drawable.ic_like));
                            btnDislike.setBackground(context.getDrawable(R.drawable.dislike_cinza));
                        }
                    });

                    btnDislike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imagem.setAvaliada(false);
                            btnDislike.setBackground(context.getDrawable(R.drawable.ic_dislike));
                            btnLike.setBackground(context.getDrawable(R.drawable.like_cinza));
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            imagemProduto.setBackground(context.getDrawable(R.drawable.produto_sem_imagem));
        }

        return view;

    }
}