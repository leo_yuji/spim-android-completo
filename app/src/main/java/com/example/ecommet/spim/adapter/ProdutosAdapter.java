package com.example.ecommet.spim.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.ecommet.spim.R;
import com.example.ecommet.spim.modelo.Imagem;
import com.example.ecommet.spim.modelo.Produto;
import com.example.ecommet.spim.utils.Prefs;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ecommet on 31/03/2017.
 */

public class ProdutosAdapter extends RecyclerView.Adapter<ProdutosAdapter.ProdutosViewHolder> {

    private List<Produto> listProdutos;
    private String fotoArrayString;
    private Context mContext;
    private List<Imagem> listImagens;
    URL urls = null;

    public ProdutosAdapter(Context mContext, List<Produto> produtos) {
        this.listProdutos = produtos;
        this.mContext = mContext;
    }


    public class ProdutosViewHolder extends RecyclerView.ViewHolder {


        private TextView nomeProdutoTxt, quantidadeCoinsProdutos, valorPorcentagem;
        private ProgressBar progress;
        private ImageView imageView;

        public ProdutosViewHolder(View itemView) {
            super(itemView);
            nomeProdutoTxt = (TextView) itemView.findViewById(R.id.nomeProduto);
            quantidadeCoinsProdutos = (TextView) itemView.findViewById(R.id.quantidadeCoinsProduto);
            valorPorcentagem = (TextView) itemView.findViewById(R.id.valorPorcentagem);
            progress = (ProgressBar) itemView.findViewById(R.id.progress_bar);
            imageView = (ImageView) itemView.findViewById(R.id.image_view_flipper);


        }


    }


    @Override
    public ProdutosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_produtos_cads, parent, false);
        ProdutosViewHolder holder = new ProdutosViewHolder(v);


        return holder;
    }

    @Override
    public void onBindViewHolder(final ProdutosAdapter.ProdutosViewHolder holder, final int position) {
        final Produto produto = listProdutos.get(position);

        holder.nomeProdutoTxt.setText(produto.getNome());

        Locale brasil = new Locale("pt", "BR");
        DecimalFormat numberFormat = new DecimalFormat("#0.0", new DecimalFormatSymbols(brasil));


        numberFormat.setParseBigDecimal(false);
        numberFormat.setDecimalSeparatorAlwaysShown(false);
        numberFormat.setMinimumFractionDigits(0);

        if (produto.getPreco() != null) {
            holder.quantidadeCoinsProdutos.setText(String.valueOf(numberFormat.format(produto.getPreco()) + " Coins"));
        } else {
            holder.quantidadeCoinsProdutos.setText("0.0");
        }

        holder.valorPorcentagem.setText(String.valueOf(produto.getProgresso()) + "% completo");
        holder.progress.setProgress(produto.getProgresso());


        listImagens = produto.getImagens();

        if (listImagens.size() != 0) {
            for (Imagem imagem : listImagens) {

                Log.e("1PROD", produto.getNome());
                Log.e("1IMG " + imagem.getId(), imagem.getCaminho());
                fotoArrayString = Prefs.getEndereco(mContext) + imagem.getCaminho();
                if (imagem.isAvaliada() == null) {

                    Log.e("2PROD", produto.getNome());
                    Log.e("2IMG " + imagem.getId(), imagem.getCaminho());
                    String erro = "";
                    try {
                        urls = new URL(fotoArrayString);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    try {
                        Bitmap image = BitmapFactory.decodeStream(urls.openConnection().getInputStream());
                        holder.imageView.setImageBitmap(image);
                        break;
                    } catch (IOException e) {
                        holder.imageView.setImageDrawable(mContext.getDrawable(R.drawable.produto_sem_imagem));
                    }

                } else {
                    holder.imageView.setImageDrawable(mContext.getDrawable(R.drawable.produto_sem_imagem));
                }

            }
        } else {
            holder.imageView.setImageDrawable(mContext.getDrawable(R.drawable.produto_sem_imagem));
        }

    }


    @Override
    public int getItemCount() {
        return this.listProdutos != null ? this.listProdutos.size() : 0;
    }


}
