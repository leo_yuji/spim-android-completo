package com.example.ecommet.spim.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.activities.MainActivity;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.webservice.UsuarioService;

import static android.app.Activity.RESULT_OK;
import static com.example.ecommet.spim.utils.Notifica.toastCurto;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class AlterarDadosPessoaisFragment extends Fragment {
    private EditText nomeUsuario, sobrenomeUsuario, cpfUsuario, tellUsuario, cellUsuario, emailUsuario;
    private String nomeUsuarioString, sobrenomeUsuarioString, tellUsuarioString, cellUsuarioString, emailUsuarioString;
    private Button buttonAlterarDados;
    private Colaborador colaborador=null;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Dados Pessoais");

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alterar_dados_pessoais, container, false);


        if (getActivity().getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }



        nomeUsuario = (EditText) view.findViewById(R.id.edit_NomeUsuario);
        nomeUsuario.setText(colaborador.getNome());
        sobrenomeUsuario = (EditText) view.findViewById(R.id.editSobrenomeUsuario);
        sobrenomeUsuario.setText(colaborador.getSobrenome());
        cpfUsuario = (EditText) view.findViewById(R.id.editCPFUsuario);
        cpfUsuario.setText(colaborador.getCpf());
        tellUsuario = (EditText) view.findViewById(R.id.editTellUsuario);
        tellUsuario.setText(colaborador.getTelefone());
        cellUsuario = (EditText) view.findViewById(R.id.editCellUsuario);
        cellUsuario.setText(colaborador.getCelular());
        emailUsuario = (EditText) view.findViewById(R.id.editEmailUsuario);
        emailUsuario.setText(colaborador.getEmail());

        buttonAlterarDados = (Button) view.findViewById(R.id.btAlteraDadosUsuario);
        buttonAlterarDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomeUsuarioString = nomeUsuario.getText().toString().trim();
                sobrenomeUsuarioString = sobrenomeUsuario.getText().toString().trim();
                tellUsuarioString = tellUsuario.getText().toString().trim();
                cellUsuarioString = cellUsuario.getText().toString().trim();
                emailUsuarioString = emailUsuario.getText().toString().trim();

                colaborador.setNome(nomeUsuarioString);
                colaborador.setSobrenome(sobrenomeUsuarioString);
                colaborador.setTelefone(tellUsuarioString);
                colaborador.setCelular(cellUsuarioString);
                colaborador.setEmail(emailUsuarioString);



                new alterarDadosTask().execute();
            }
        });



        return view;
    }


    public class alterarDadosTask extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... params) {
            try {
                return UsuarioService.alterarDados(getActivity(), colaborador);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            if (s == null) {
                toastCurto(getActivity(), "Erro ao realizar envio de foto");
            } else {

                toastCurto(getActivity(), "Atualizado com sucesso");

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("colaborador", colaborador);
                getActivity().setResult(RESULT_OK, intent);
                startActivity(intent);

            }
        }


    }

}
