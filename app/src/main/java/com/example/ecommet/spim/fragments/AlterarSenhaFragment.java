package com.example.ecommet.spim.fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.activities.MainActivity;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.utils.Prefs;
import com.example.ecommet.spim.webservice.UsuarioService;

import static android.app.Activity.RESULT_OK;
import static com.example.ecommet.spim.utils.Notifica.toastCurto;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class AlterarSenhaFragment extends Fragment {
    private EditText senhaAtualUsuario, novaSenhaUsuario, comfimarSenhaUsuario;
    private String senhaAtualUsuarioString, novaSenhaUsuarioString, comfimarSenhaUsuarioString;
    private Button buttonAlterarSenha;


    Colaborador colaborador = null;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Alterar Senha");

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alterar_senha_usuario, container, false);


        if (getActivity().getIntent().hasExtra("colaborador")) {
            this.colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }

        senhaAtualUsuario = (EditText) view.findViewById(R.id.senhaAtualUsuario);
        novaSenhaUsuario = (EditText) view.findViewById(R.id.novaSenhaUsuario);
        comfimarSenhaUsuario = (EditText) view.findViewById(R.id.comfirmarSenhaUsuario);

        buttonAlterarSenha = (Button) view.findViewById(R.id.btAlterarSenha);
        buttonAlterarSenha.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                senhaAtualUsuarioString = senhaAtualUsuario.getText().toString().trim();
                novaSenhaUsuarioString = novaSenhaUsuario.getText().toString().trim();
                comfimarSenhaUsuarioString = comfimarSenhaUsuario.getText().toString().trim();
                String senhaAtualCorreta = Prefs.getAcesso(getContext())[1].toString();
                if (!senhaAtualUsuarioString.equals(senhaAtualCorreta)) {
                    senhaAtualUsuario.requestFocus();
                    senhaAtualUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                } else {

                    if (!novaSenhaUsuarioString.equals(comfimarSenhaUsuarioString)) {
                        comfimarSenhaUsuario.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                        comfimarSenhaUsuario.requestFocus();

                    } else {
                        colaborador.setSenha(comfimarSenhaUsuarioString);
                        new alterarSenhaTask().execute();

                    }

                }


            }


        });


        return view;
    }


    public class alterarSenhaTask extends AsyncTask<Void, Void, String> {
        private String erroStatus = "";

        @Override
        protected String doInBackground(Void... params) {
            try {
                return UsuarioService.alterarSenha(getActivity(), colaborador, senhaAtualUsuarioString);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                erroStatus = e.getMessage();
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {

            if (s == null || erroStatus.equals("500") || erroStatus.equals("400") || erroStatus.equals("405")) {
                toastCurto(getActivity(), "Erro ao alterar senha!");
            } else {
                toastCurto(getActivity(), "Atualizado com sucesso");

                Prefs.gravarLogin(String.valueOf(colaborador.getEmail()), String.valueOf(colaborador.getSenha().toString()), getActivity());

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("colaborador", colaborador);
                getActivity().setResult(RESULT_OK, intent);
                startActivity(intent);
            }
        }


    }


}
