package com.example.ecommet.spim.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.adapter.HistoricoTransacoesAdapter;
import com.example.ecommet.spim.interfaces.GridSpacingItemDecoration;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.MovimentoCredito;
import com.example.ecommet.spim.utils.CustomDialog;
import com.example.ecommet.spim.webservice.HistoricoMovimentosService;

import java.util.List;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class CarteiraFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private TextView quantidadeCreditoUsuario;
    private RecyclerView recyclerView;
    private int buscarMovimentoPorFilter = 0;
    private List<MovimentoCredito> movimentoCreditoList;
    private HistoricoTransacoesAdapter adapterHistoricoTransacoes;
    private String tipoTransferenciaString, dataRecentesString;
    private AlertDialog dialogFilterMoviment = null;
    private SwipeRefreshLayout swipeRefreshTransacoes;
    private RadioButton radioButtonEntrada;
    private RadioButton radioButtonRecentes;
    private RadioButton radioButtonSaida;
    private RadioButton radioButtonAntigos;
    private ProgressBar progressBar;
    private Boolean isSwipeRefresh = false;
    private FloatingActionButton fab;
    private TextView txtListaVazia;

    Colaborador colaborador = null;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Histórico de Transações");

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogHistorico(view);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_historico_transacoes, container, false);


        if (getActivity().getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }

        txtListaVazia = (TextView) view.findViewById(R.id.lista_historico_vazia);

        quantidadeCreditoUsuario = (TextView) view.findViewById(R.id.creditosUsuario);
        quantidadeCreditoUsuario.setText(colaborador.getCredito() + " e-coins");

        //progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_HistoricoTransacoes);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        GridSpacingItemDecoration gridSpacingItemDecoration = new GridSpacingItemDecoration(1, 2, true);
        gridSpacingItemDecoration.dpToPx(2);
        recyclerView.addItemDecoration(gridSpacingItemDecoration);

        swipeRefreshTransacoes = (SwipeRefreshLayout) view.findViewById(R.id.swipeListaTransacoes);

        swipeRefreshTransacoes.setOnRefreshListener(this);
        swipeRefreshTransacoes.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);

        atualizarListaDeTransacoes();
        return view;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        isSwipeRefresh = true;
        atualizarListaDeTransacoes();
    }

    private void atualizarListaDeTransacoes() {
        new BuscarMovimentoHistoricoTipoTask().execute();
    }

    private void dialogHistorico(View view) {


        // Creating alert Dialog with one Button
        AlertDialog.Builder builderMTransferencias = new AlertDialog.Builder(getContext());


        //Métdo que queria um dialog que cadastra uma tarefa;
        LayoutInflater layoutInflaterMTransferencias = LayoutInflater.from(getActivity());
        View mViewMTransferencias = layoutInflaterMTransferencias.inflate(R.layout.dialog_ordenar_historico, null);
        builderMTransferencias.setView(mViewMTransferencias);

        radioButtonEntrada = (RadioButton) mViewMTransferencias.findViewById(R.id.rb_tipoEntrada);
        radioButtonRecentes = (RadioButton) mViewMTransferencias.findViewById(R.id.rb_dataRecentes);

        radioButtonSaida = (RadioButton) mViewMTransferencias.findViewById(R.id.rb_tipoSaida);
        radioButtonAntigos = (RadioButton) mViewMTransferencias.findViewById(R.id.rb_dataAntigos);

        final RadioGroup tipoTransferencia = (RadioGroup) mViewMTransferencias.findViewById(R.id.rg_tipo);
        final RadioGroup tipoData = (RadioGroup) mViewMTransferencias.findViewById(R.id.rg_data);


        tipoTransferencia.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int botaoSelecionado) {

                switch (botaoSelecionado) {
                    case R.id.rb_tipoEntrada:
                        tipoTransferenciaString = "ENTRADA";
                        radioButtonRecentes.setEnabled(true);
                        radioButtonAntigos.setEnabled(true);
                        break;
                    case R.id.rb_tipoSaida:
                        tipoTransferenciaString = "SAÍDA";
                        radioButtonRecentes.setEnabled(true);
                        radioButtonAntigos.setEnabled(true);
                        break;
                    default:
                        break;
                }
            }
        });

        tipoData.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int botaoSelecionado) {


                switch (botaoSelecionado) {
                    case R.id.rb_dataRecentes:
                        dataRecentesString = "MAIS RECENTES";
                        dialogFilterMoviment.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        break;
                    case R.id.rb_dataAntigos:
                        dataRecentesString = "MAIS ANTIGOS";
                        dialogFilterMoviment.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        break;
                    default:
                        break;

                }
            }
        });


        builderMTransferencias.setPositiveButton("APLICAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (tipoTransferenciaString != null && dataRecentesString != null) {
                    if (tipoTransferenciaString.equals("ENTRADA") && dataRecentesString.equals("MAIS ANTIGOS")) {
                        buscarMovimentoPorFilter = 1;
                    } else if (tipoTransferenciaString.equals("ENTRADA") && dataRecentesString.equals("MAIS RECENTES")) {
                        buscarMovimentoPorFilter = 2;
                    } else if (tipoTransferenciaString.equals("SAÍDA") && dataRecentesString.equals("MAIS ANTIGOS")) {

                        buscarMovimentoPorFilter = 3;

                    } else if (tipoTransferenciaString.equals("SAÍDA") && dataRecentesString.equals("MAIS RECENTES")) {

                        buscarMovimentoPorFilter = 4;

                    }

                }


                new BuscarMovimentoHistoricoTipoTask().execute();
            }

        });

        builderMTransferencias.setNegativeButton("VOLTAR", new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogFilterMoviment.dismiss();
            }
        });


        dialogFilterMoviment = builderMTransferencias.create();

        dialogFilterMoviment.show();
        dialogFilterMoviment.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
    }


    private class BuscarMovimentoHistoricoTipoTask extends AsyncTask<Void, Void, List<MovimentoCredito>> {
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isSwipeRefresh) {
                customDialog = new CustomDialog(getContext());
                customDialog.show();
            }
        }

        @Override
        protected List<MovimentoCredito> doInBackground(Void... params) {
            try {

                return movimentoCreditoList = HistoricoMovimentosService.listarMovimentosBuscar(getActivity(), buscarMovimentoPorFilter);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();


                return null;
            }
        }

        @Override
        protected void onPostExecute(List<MovimentoCredito> movimentoCreditos) {
            super.onPostExecute(movimentoCreditos);


            swipeRefreshTransacoes.setRefreshing(false);
            if (movimentoCreditoList != null) {
                if (!movimentoCreditoList.isEmpty()) {
                    fab.setVisibility(View.VISIBLE);
                    txtListaVazia.setVisibility(View.GONE);
                } else {
                    fab.setVisibility(View.GONE);
                    txtListaVazia.setVisibility(View.VISIBLE);
                }

                adapterHistoricoTransacoes = new HistoricoTransacoesAdapter(getActivity(), movimentoCreditoList);
                recyclerView.setAdapter(adapterHistoricoTransacoes);

            } else {
                fab.setVisibility(View.GONE);
                txtListaVazia.setVisibility(View.GONE);

            }

            if (!isSwipeRefresh) {
                customDialog.dismiss();
            }

            isSwipeRefresh = false;


        }

    }


}
