package com.example.ecommet.spim.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;


import com.example.ecommet.spim.R;
import com.example.ecommet.spim.activities.PreencherProdutoActivity;
import com.example.ecommet.spim.adapter.ProdutosAdapter;
import com.example.ecommet.spim.interfaces.GridSpacingItemDecoration;
import com.example.ecommet.spim.interfaces.RecyclerItemClickListener;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Produto;
import com.example.ecommet.spim.utils.CustomDialog;
import com.example.ecommet.spim.webservice.ProdutoService;

import java.util.List;

import static android.view.View.GONE;
import static com.example.ecommet.spim.utils.Notifica.toastCurto;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class ListaProdutosPreencherFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerView;
    private List<Produto> produtosList;
    private ProdutosAdapter adapterProduto;
    private String nomeProduto;
    private Colaborador colaborador = null;
    private AlertDialog dialogFilterProdutos;
    private int buscarProdutosPorFilter = 0;
    private String ordenarRGString;
    private final int ATUALIZAR_LISTA_CODE = 300;
    private SwipeRefreshLayout swipeRefreshProdutos;
    private boolean isSwipeRefresh = false;
    private FloatingActionButton fab;
    private TextView txtListaVazia;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Preencher Produto");

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_produtos, container, false);


        if (getActivity().getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exibeDialogFilterProdutos(view);
            }
        });

        txtListaVazia = (TextView) view.findViewById(R.id.lista_produtos_vazia);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_Produtos);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {


                        Produto produtoPreencher = produtosList.get(position);

                        if (produtoPreencher != null) {
                            Intent intent = new Intent(getContext(), PreencherProdutoActivity.class);
                            intent.putExtra("produto", produtoPreencher);
                            intent.putExtra("colaborador", colaborador);
                            startActivityForResult(intent, ATUALIZAR_LISTA_CODE);

                        } else {
                            toastCurto(getActivity(), "Não Achou o produto");
                        }


                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(mLayoutManager);

        GridSpacingItemDecoration gridSpacingItemDecoration = new GridSpacingItemDecoration(1, 2, true);
        gridSpacingItemDecoration.dpToPx(2);
        recyclerView.addItemDecoration(gridSpacingItemDecoration);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        swipeRefreshProdutos = (SwipeRefreshLayout) view.findViewById(R.id.swipeListaProdutos);

        swipeRefreshProdutos.setOnRefreshListener(this);
        swipeRefreshProdutos.setColorSchemeResources( R.color.colorPrimary, R.color.colorAccent);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */

        atualizarListaDeProdutos();

        setHasOptionsMenu(true);


        return view;
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        isSwipeRefresh = true;
        atualizarListaDeProdutos();
    }

    private void atualizarListaDeProdutos() {

        new BuscarProdutoTipoTask().execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);


        MenuItem mSearchMenuItem = menu.findItem(R.id.search_view);


        SearchView searchView = (SearchView) mSearchMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //populaLista(query);


                nomeProduto = query;
                new BuscarProdutoNomeTask().execute();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    new BuscarProdutoTipoTask().execute();
                }
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.search_view:
                return true;
            default:
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        new BuscarProdutoTipoTask().execute();
    }

    private void exibeDialogFilterProdutos(View view) {


        // Creating alert Dialog with one Button
        AlertDialog.Builder builderDialogOrdenarProdutos = new AlertDialog.Builder(getContext());


        //Métdo que queria um dialog que cadastra uma tarefa;
        LayoutInflater layoutInflaterDialogOrdenarProdutos = LayoutInflater.from(getActivity());
        View mViewDialogOrdenarProdutos = layoutInflaterDialogOrdenarProdutos.inflate(R.layout.dialog_ordenar_produtos, null);
        builderDialogOrdenarProdutos.setView(mViewDialogOrdenarProdutos);


        final RadioGroup ordenar_rg = (RadioGroup) mViewDialogOrdenarProdutos.findViewById(R.id.RG_ORDENAR);


        ordenar_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int botaoSelecionado) {


                switch (botaoSelecionado) {
                    case R.id.rb_remuneracaoMaior:
                        ordenar_rg.check(R.id.rb_remuneracaoMaior);
                        ordenarRGString = "MAIOR";
                        break;
                    case R.id.rb_remuneracaoMenor:
                        ordenar_rg.check(R.id.rb_remuneracaoMenor);
                        ordenarRGString = "MENOR";
                        break;

                    case R.id.rb_dataRecentes:
                        ordenar_rg.check(R.id.rb_dataRecentes);
                        ordenarRGString = "MAIS ANTIGOS";


                        break;
                    case R.id.rb_dataAntigos:
                        ordenar_rg.check(R.id.rb_dataAntigos);
                        ordenarRGString = "MAIS RECENTES";
                        break;

                    case R.id.rb_alfabetica:
                        ordenar_rg.check(R.id.rb_alfabetica);
                        ordenarRGString = "ALFABÉTICAS";
                        break;

                    default:
                        break;
                }
            }
        });


        builderDialogOrdenarProdutos.setPositiveButton("APLICAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (ordenarRGString.equals("MAIOR")) {
                    buscarProdutosPorFilter = 1;
                } else if (ordenarRGString.equals("MENOR")) {

                    buscarProdutosPorFilter = 2;
                } else if (ordenarRGString.equals("MAIS RECENTES")) {
                    buscarProdutosPorFilter = 3;
                } else if (ordenarRGString.equals("MAIS ANTIGOS")) {

                    buscarProdutosPorFilter = 4;

                } else if (ordenarRGString.equals("ALFABÉTICAS")) {
                    buscarProdutosPorFilter = 5;
                }
                new BuscarProdutoTipoTask().execute();

            }

        });

        builderDialogOrdenarProdutos.setNegativeButton("VOLTAR", new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogFilterProdutos.dismiss();
            }
        });


        dialogFilterProdutos = builderDialogOrdenarProdutos.create();

        dialogFilterProdutos.show();
    }


    private class BuscarProdutoNomeTask extends AsyncTask<Void, Void, List<Produto>> {


        @Override
        protected List<Produto> doInBackground(Void... params) {
            try {
                return produtosList = ProdutoService.buscarProdutosNome(getActivity(), nomeProduto);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Produto> s) {


            if (s == null) {
                toastCurto(getActivity(), getString(R.string.erro_enviar));
            } else {
                if (produtosList != null) {
                    adapterProduto = new ProdutosAdapter(getActivity(), produtosList);

                    recyclerView.setAdapter(adapterProduto);


                }


            }
        }
    }

    private class BuscarProdutoTipoTask extends AsyncTask<Void, Void, List<Produto>> {
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isSwipeRefresh) {
                customDialog = new CustomDialog(getContext());
                customDialog.show();
            }
        }

        @Override
        protected List<Produto> doInBackground(Void... params) {
            try {
                return produtosList = ProdutoService.listarProdutosPorBusca(getActivity(), buscarProdutosPorFilter);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();


                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Produto> produtos) {
            // atualiza a lista
            swipeRefreshProdutos.setRefreshing(false);
            if (produtosList != null) {
                if (!produtosList.isEmpty()) {
                    fab.setVisibility(View.VISIBLE);
                    txtListaVazia.setVisibility(GONE);
                } else {
                    fab.setVisibility(GONE);
                    txtListaVazia.setVisibility(View.VISIBLE);
                }
                adapterProduto = new ProdutosAdapter(getActivity(), produtosList);
                recyclerView.setAdapter(adapterProduto);
            } else {
                fab.setVisibility(GONE);
                txtListaVazia.setVisibility(View.GONE);

            }

            if (!isSwipeRefresh) {
                customDialog.dismiss();
            }
            isSwipeRefresh = false;

        }

    }


}


