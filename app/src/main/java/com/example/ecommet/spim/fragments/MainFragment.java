package com.example.ecommet.spim.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.interfaces.RoundedImageView;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.utils.Prefs;
import com.squareup.picasso.Picasso;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class MainFragment extends Fragment {
    private TextView nameUsuario;
    private ImageButton btPreencherProdutoFragment, btAvaliarProdutoFragment;
    public Colaborador colaborador = null;
    private RoundedImageView imagePerfilUsuario;
    private View viewHeader;
    private NavigationView navigationView;
    private TextView nameUsuarioMenu, creditoUsuario, rakingUsuario;
    public Context context = null;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Ecommet");


    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        if (getActivity().getIntent().hasExtra("colaborador")) {
            this.colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }

        navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);

        MenuItem itemAvaliar = navigationView.getMenu().findItem(R.id.nav_avaliarProdutos);
        if (itemAvaliar != null) {
            if (colaborador.getRanking() < 50) {
                itemAvaliar.setVisible(false);
            } else {
                itemAvaliar.setVisible(true);
            }
        }

        viewHeader = navigationView.getHeaderView(0);

        nameUsuarioMenu = (TextView) viewHeader.findViewById(R.id.nav_header_textName);
        creditoUsuario = (TextView) viewHeader.findViewById(R.id.nav_header_textCreditosUser);
        rakingUsuario = (TextView) viewHeader.findViewById(R.id.nav_header_textRaking);

        nameUsuario = (TextView) view.findViewById(R.id.nomeUsuario);
        nameUsuario.setText(getString(R.string.nome_usuario, colaborador.getNome()));

        imagePerfilUsuario = RoundedImageView.class.cast(view.findViewById(R.id.image_itemMain));


        btPreencherProdutoFragment = (ImageButton) view.findViewById(R.id.bt_PreencherProdutos);
        btAvaliarProdutoFragment = (ImageButton) view.findViewById(R.id.bt_AvaliarProdutos);

        btPreencherProdutoFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fts = getFragmentManager().beginTransaction();
                fts.replace(R.id.content_main, new ListaProdutosPreencherFragment());
                fts.commit();
            }
        });

        if (colaborador.getRanking() < 50) {
            btAvaliarProdutoFragment.setVisibility(View.GONE);
        } else {
            btAvaliarProdutoFragment.setVisibility(View.VISIBLE);
        }

        btAvaliarProdutoFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction fts = getFragmentManager().beginTransaction();
                fts.replace(R.id.content_main, new ListaProdutosAvaliarFragment());
                fts.commit();
            }
        });

        onResume();
        return view;

    }


    @Override
    public void onResume() {
        super.onResume();

        if (colaborador.getRanking() < 50) {
            btAvaliarProdutoFragment.setVisibility(View.GONE);
        } else {
            btAvaliarProdutoFragment.setVisibility(View.VISIBLE);
        }
        String fotoPerfilString = null;
        if (getContext() == null) {

            if (context != null) {
                fotoPerfilString = Prefs.getEndereco(context) + colaborador.getCaminhoImagem();
            }

        } else {
            fotoPerfilString = Prefs.getEndereco(getContext()) + colaborador.getCaminhoImagem();
        }

        if (fotoPerfilString != null) {
            Picasso.with(getContext()).load(fotoPerfilString).error(R.drawable.logo).into(imagePerfilUsuario);
        }

    }



}
