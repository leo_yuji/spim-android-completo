package com.example.ecommet.spim.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ecommet.spim.R;
import com.example.ecommet.spim.activities.MainActivity;
import com.example.ecommet.spim.modelo.Banco;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Conta;
import com.example.ecommet.spim.modelo.MovimentoCredito;
import com.example.ecommet.spim.modelo.TipMovement;
import com.example.ecommet.spim.utils.CustomDialog;
import com.example.ecommet.spim.webservice.BancoService;
import com.example.ecommet.spim.webservice.HistoricoMovimentosService;
import com.example.ecommet.spim.webservice.UsuarioService;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import java.math.BigDecimal;
import java.util.List;

import static com.example.ecommet.spim.utils.Notifica.DialogMensagem;
import static com.example.ecommet.spim.utils.Notifica.toastCurto;
import static java.lang.Double.parseDouble;

/**
 * Created by Ecommet on 28/03/2017.
 */

public class RetirarCreditosFragment extends Fragment {

    private int contador = 0;
    private Double valorParaRetiradaEntrada, valorParaRetiradaConvertido;
    private TextView creditosUsuarioTxt;
    private EditText valorSolicitadoEdit, valorSolicitadoConvertidoEdit, agenciaEdit, contaEdit, cpfEdit, nomeContaEdit;
    private ImageButton btAdcValor, btDiminuirValor;
    private Button btSolicitarRetirada;
    private Colaborador colaborador = null;
    private static int SPLASH_TIME_OUT = 2000;
    private MovimentoCredito movimentoCreditoSolicitar;
    private Spinner spinnerBancos;
    private LinearLayout layoutCriarConta;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Retirar Crédito");


    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_retirar_creditos, container, false);

        if (getActivity().getIntent().hasExtra("colaborador")) {
            colaborador = (Colaborador) getActivity().getIntent().getSerializableExtra("colaborador");

        }

        layoutCriarConta = (LinearLayout) view.findViewById(R.id.layout_criar_conta);

        if (colaborador.getConta() != null) {
            layoutCriarConta.setVisibility(View.GONE);
        } else {
            layoutCriarConta.setVisibility(View.VISIBLE);
        }


        creditosUsuarioTxt = (TextView) view.findViewById(R.id.creditosUsuario);
        creditosUsuarioTxt.setText(colaborador.getCredito() + " e-coins");

        valorSolicitadoEdit = (EditText) view.findViewById(R.id.valorSolicitadoRetirada);
        valorSolicitadoConvertidoEdit = (EditText) view.findViewById(R.id.valorConvertidoRetirada);

        btAdcValor = (ImageButton) view.findViewById(R.id.bt_adcValor);
        btDiminuirValor = (ImageButton) view.findViewById(R.id.bt_diminuirValor);

        spinnerBancos = (Spinner) view.findViewById(R.id.spinnerBanco);
        agenciaEdit = (EditText) view.findViewById(R.id.agenciaEdit);
        contaEdit = (EditText) view.findViewById(R.id.contaEdit);
        nomeContaEdit = (EditText) view.findViewById(R.id.nomeContaEdit);
        cpfEdit = (EditText) view.findViewById(R.id.cpfContaEdit);
        cpfEdit.addTextChangedListener(new PatternedTextWatcher("###.###.###-##"));

        agenciaEdit.addTextChangedListener(new PatternedTextWatcher("####"));

        btSolicitarRetirada = (Button) view.findViewById(R.id.bt_confirmarRetirada);


        valorSolicitadoEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (valorSolicitadoEdit.getText().toString().trim().equals("0")) {
                    valorSolicitadoEdit.setText("");
                }

                if (!hasFocus) {
                    converterCredito();
                }
            }

        });


        valorSolicitadoEdit.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    converterCredito();
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        btAdcValor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valorSolicitadoEdit.getText().toString().trim().equals("")) {
                    contador = 0;

                } else {
                    contador = Integer.valueOf(valorSolicitadoEdit.getText().toString().trim());

                    if (contador < colaborador.getCredito().intValue()) {
                        contador++;
                        btSolicitarRetirada.setEnabled(true);
                    } else {
                        contador = 0;
                        toastCurto(getActivity(), "Créditos insuficientes!");
                        btSolicitarRetirada.setEnabled(false);
                    }
                }


                valorSolicitadoEdit.setText(contador + "");

                valorParaRetiradaEntrada = parseDouble(valorSolicitadoEdit.getText().toString().trim());

                if (valorParaRetiradaEntrada > 10) {
                    valorParaRetiradaConvertido = valorParaRetiradaEntrada - 10;
                    valorSolicitadoConvertidoEdit.setText(String.valueOf(valorParaRetiradaConvertido));
                    btSolicitarRetirada.setEnabled(true);
                } else {
                    toastCurto(getActivity(), "Insira um valor maior que dez");
                    btSolicitarRetirada.setEnabled(false);
                }
            }
        });


        btDiminuirValor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valorSolicitadoEdit.getText().toString().trim().equals("")) {
                    contador = 0;

                } else {
                    contador = Integer.valueOf(valorSolicitadoEdit.getText().toString().trim());
                    if (contador != 0) {
                        contador--;
                        btSolicitarRetirada.setEnabled(true);
                    } else {
                        toastCurto(getActivity(), "Valor menor que seus créditos");
                        btSolicitarRetirada.setEnabled(false);
                    }
                }


                valorSolicitadoEdit.setText(contador + "");
                valorParaRetiradaEntrada = parseDouble(valorSolicitadoEdit.getText().toString().trim());

                if (valorParaRetiradaEntrada > 10) {
                    valorParaRetiradaConvertido = valorParaRetiradaEntrada - 10;
                    valorSolicitadoConvertidoEdit.setText(String.valueOf(valorParaRetiradaConvertido));
                    btSolicitarRetirada.setEnabled(true);
                } else {
                    toastCurto(getActivity(), "Insira um valor maior que dez");
                    btSolicitarRetirada.setEnabled(false);
                }
            }
        });


        btSolicitarRetirada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (colaborador.getCredito().doubleValue() > 0 && colaborador.getCredito().doubleValue() >= valorParaRetiradaEntrada.doubleValue()) {
                    if (valorParaRetiradaEntrada != 0 && valorParaRetiradaConvertido != null) {


                        if (!valorParaRetiradaConvertido.equals(0)) {
                            btSolicitarRetirada.setEnabled(false);
                            new Handler().postDelayed(new Runnable() {
                                /*
                                 * Exibindo splash com um timer.
                                 */
                                @Override
                                public void run() {

                                    RetiradaDeCreditos();
                                }
                            }, SPLASH_TIME_OUT);

                        } else {
                            toastCurto(getActivity(), "Insira um valor");

                        }


                    } else {
                        toastCurto(getActivity(), "Insira um valor");
                    }

                } else {
                    toastCurto(getActivity(), "Créditos insuficientes!");
                }

            }
        });


        if (colaborador.getConta() != null) {
            nomeContaEdit.setText(colaborador.getConta().getTitular());
            cpfEdit.setText(colaborador.getConta().getCpfTitular());
            agenciaEdit.setText(colaborador.getConta().getAgencia());
            contaEdit.setText(colaborador.getConta().getConta() + "/" + colaborador.getConta().getDigito());

            spinnerBancos.setSelection(Integer.parseInt(String.valueOf(colaborador.getConta().getBanco().getId() - 1)));


        }

        new BuscarBancosTask().execute();


        return view;
    }

    private void converterCredito() {
        if (valorSolicitadoEdit.getText().toString().trim().equals("")) {
            contador = 0;

        } else {
            contador = Integer.valueOf(valorSolicitadoEdit.getText().toString().trim());
            if (contador == colaborador.getCredito().intValue() && contador <= colaborador.getCredito().intValue()) {
                btSolicitarRetirada.setEnabled(true);
                contador += contador;
            } else if (contador >= colaborador.getCredito().intValue()) {
                btSolicitarRetirada.setEnabled(false);
                contador = 0;
                toastCurto(getActivity(), "Créditos insuficientes!");

            }

            //valorSolicitadoEdit.setText(contador + "");
            valorParaRetiradaEntrada = parseDouble(valorSolicitadoEdit.getText().toString().trim());

            if (valorParaRetiradaEntrada > 10) {
                valorParaRetiradaConvertido = valorParaRetiradaEntrada - 10;
                valorSolicitadoConvertidoEdit.setText(String.valueOf(valorParaRetiradaConvertido));
                btSolicitarRetirada.setEnabled(true);
            } else {
                toastCurto(getActivity(), "Insira um valor maior que dez");
                btSolicitarRetirada.setEnabled(false);
            }

        }

    }


    private void RetiradaDeCreditos() {

        if (spinnerBancos.getSelectedItem().toString().isEmpty() || cpfEdit.getText().toString().trim().isEmpty() ||
                agenciaEdit.getText().toString().trim().isEmpty() || contaEdit.getText().toString().trim().isEmpty()
                ) {
            Toast.makeText(getActivity(), "Preencher todos os campos.", Toast.LENGTH_SHORT).show();
        } else {

            if (colaborador.getConta() != null) {

                movimentoCreditoSolicitar = new MovimentoCredito();
                movimentoCreditoSolicitar.setEcoins(BigDecimal.valueOf(valorParaRetiradaEntrada));
                movimentoCreditoSolicitar.setValor(BigDecimal.valueOf(valorParaRetiradaConvertido));
                movimentoCreditoSolicitar.setTipoMovimento(TipMovement.SAIDA);
                new SolicitarRetirdaTask().execute();
            } else {

                Conta conta = new Conta();

                conta.setTitular(nomeContaEdit.getText().toString().trim());
                conta.setCpfTitular(cpfEdit.getText().toString().trim());
                conta.setAgencia(agenciaEdit.getText().toString().trim());
                String contaText = contaEdit.getText().toString().trim();

                if (contaText.contains("/")) {
                    String novaConta = contaText.split("/")[0];
                    conta.setConta(novaConta);
                    String digito = contaText.split("/")[1];
                    conta.setDigito(digito);
                } else {
                    conta.setConta(contaText);
                    conta.setDigito("0");
                }

                Banco bancoSelecionado = (Banco) spinnerBancos.getSelectedItem();
                conta.setBanco(bancoSelecionado);

                colaborador.setConta(conta);

                movimentoCreditoSolicitar = new MovimentoCredito();
                movimentoCreditoSolicitar.setEcoins(BigDecimal.valueOf(valorParaRetiradaEntrada));
                movimentoCreditoSolicitar.setValor(BigDecimal.valueOf(valorParaRetiradaConvertido));
                movimentoCreditoSolicitar.setTipoMovimento(TipMovement.SAIDA);


                new CadastrarContaUsuario().execute();
            }
        }
    }

    public class CadastrarContaUsuario extends AsyncTask<Void, Void, String> {
        private String erroStatus = "";
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customDialog = new CustomDialog(getContext());
            customDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return UsuarioService.criarConta(getActivity(), colaborador.getConta());
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                erroStatus = e.getMessage();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            customDialog.dismiss();
            btSolicitarRetirada.setEnabled(true);
            if (s == null) {

                if (!erroStatus.isEmpty()) {
                    if (erroStatus.equals("406")) {
                        toastCurto(getActivity(), "CPF já cadastrado");
                        cpfEdit.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                        cpfEdit.requestFocus();
                        cpfEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {
                                    cpfEdit.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

                                }
                            }
                        });
                    } else if (erroStatus.equals("403")) {
                        toastCurto(getActivity(), "CPF inválido");
                        cpfEdit.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.SRC_ATOP);
                        cpfEdit.requestFocus();
                        cpfEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {
                                    cpfEdit.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);

                                }
                            }
                        });
                    } else {
                        DialogMensagem(getActivity(), "Preste atenção", getString(R.string.erro_solicitar_credito));
                    }
                }

            } else {

                new Handler().postDelayed(new Runnable() {
                    /*
                     * Exibindo splash com um timer.
                     */
                    @Override
                    public void run() {
                        new SolicitarRetirdaTask().execute();

                    }
                }, SPLASH_TIME_OUT);

            }
        }
    }

    public class SolicitarRetirdaTask extends AsyncTask<Void, Void, String> {
        private CustomDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customDialog = new CustomDialog(getContext());
            customDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return HistoricoMovimentosService.solicitarRetirada(getActivity(), movimentoCreditoSolicitar);
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            customDialog.dismiss();
            btSolicitarRetirada.setEnabled(true);
            if (s == null) {
                DialogMensagem(getActivity(), "Alerta", getString(R.string.erro_solicitar_credito));


            } else {
                DialogMensagem(getActivity(), "Retirada Solicitada", " A retirada dos seus créditos foi solicitada com sucesso");
                new Handler().postDelayed(new Runnable() {
                    /*
                     * Exibindo splash com um timer.
                     */
                    @Override
                    public void run() {


                        Intent intent = new Intent(getContext(), MainActivity.class);

                        intent.putExtra("colaborador", colaborador);
                        startActivity(intent);
                    }
                }, SPLASH_TIME_OUT);

            }
        }
    }

    public class BuscarBancosTask extends AsyncTask<Void, Void, List<Banco>> {
        private List<Banco> bancoList;

        @Override
        protected List<Banco> doInBackground(Void... params) {
            try {
                return bancoList = BancoService.listBancos(getActivity());
            } catch (Exception e) {
                Log.e("ERRO", e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Banco> bancos) {
            super.onPostExecute(bancos);
            if (bancoList != null) {
                // Spinner adapter
                spinnerBancos.setAdapter(new ArrayAdapter<Banco>(
                        getActivity(), R.layout.support_simple_spinner_dropdown_item,
                        bancoList));


            } else {

                toastCurto(getActivity(), "Não há transferências!");
            }


        }

    }


}
