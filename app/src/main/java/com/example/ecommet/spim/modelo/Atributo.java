package com.example.ecommet.spim.modelo;


import java.io.Serializable;

public class Atributo implements Serializable {
	private Long id;
	private String nome;
	private TipoAtributo tipoAtributo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoAtributo getTipoAtributo() {
		return tipoAtributo;
	}
	public void setTipoAtributo(TipoAtributo tipoAtributo) {
		this.tipoAtributo = tipoAtributo;
	}


	@Override
	public String toString() {
		return  nome + " - ";
	}
}
