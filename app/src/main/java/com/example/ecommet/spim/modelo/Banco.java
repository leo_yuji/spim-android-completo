package com.example.ecommet.spim.modelo;


import java.io.Serializable;

public class Banco implements Serializable {
	

	private Long id;
	private Long codigo;
	private String nome;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}


	@Override
	public String toString() {
		return  codigo + "-" + nome;
	}
}
