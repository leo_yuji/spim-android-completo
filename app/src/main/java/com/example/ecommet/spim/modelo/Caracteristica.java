package com.example.ecommet.spim.modelo;

import java.io.Serializable;

public class Caracteristica implements Serializable{

	private Long id;
	private String valor;
	private Produto produto;
	private Usuario usuarioPreenchedor;
	private Usuario usuarioAvaliador;
	private Boolean avaliada = null;
	private Atributo atributo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public Usuario getUsuarioPreenchedor() {
		return usuarioPreenchedor;
	}

	public void setUsuarioPreenchedor(Usuario usuarioPreenchedor) {
		this.usuarioPreenchedor = usuarioPreenchedor;
	}

	public Usuario getUsuarioAvaliador() {
		return usuarioAvaliador;
	}

	public void setUsuarioAvaliador(Usuario usuarioAvaliador) {
		this.usuarioAvaliador = usuarioAvaliador;
	}

	public Boolean isAvaliada() {
		return avaliada;
	}

	public void setAvaliada(Boolean avaliada) {
		this.avaliada = avaliada;
	}

	public Atributo getAtributo() {
		return atributo;
	}

	public void setAtributo(Atributo atributo) {
		this.atributo = atributo;
	}


}
