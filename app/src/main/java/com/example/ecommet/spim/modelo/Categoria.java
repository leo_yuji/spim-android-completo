package com.example.ecommet.spim.modelo;


import java.io.Serializable;

public class Categoria implements Serializable{
	
	private Long id;
	private String nome;
	private Categoria pai;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Categoria getPai() {
		return pai;
	}
	public void setPai(Categoria pai) {
		this.pai = pai;
	}

	
}
