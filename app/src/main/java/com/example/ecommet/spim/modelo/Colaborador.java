package com.example.ecommet.spim.modelo;


import java.io.Serializable;
import java.math.BigDecimal;

public class Colaborador extends Usuario implements Serializable {
	
	private int ranking = 0;
	private BigDecimal credito = BigDecimal.valueOf(0000000000000000);
	private TokenJWT tokenJWT;
	
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public BigDecimal getCredito() {
		return credito;
	}
	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}


	public TokenJWT getTokenJWT() {
		return tokenJWT;
	}

	public void setTokenJWT(TokenJWT tokenJWT) {
		this.tokenJWT = tokenJWT;
	}

	}
