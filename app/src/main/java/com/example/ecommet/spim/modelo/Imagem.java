package com.example.ecommet.spim.modelo;


import java.io.Serializable;

public class Imagem implements Serializable {
    private Long id;

    private String caminho;
    private String imagemAndroid;
    private Boolean avaliada = null;
    private Usuario usuarioPreenchedor;
    private Usuario usuarioAvaliador;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public String getImagemAndroid() {
        return imagemAndroid;
    }

    public void setImagemAndroid(String imagemAndroid) {
        this.imagemAndroid = imagemAndroid;
    }

    public Boolean isAvaliada() {
        return avaliada;
    }

    public void setAvaliada(Boolean avaliada) {
        this.avaliada = avaliada;
    }

    public Usuario getUsuarioPreenchedor() {
        return usuarioPreenchedor;
    }

    public void setUsuarioPreenchedor(Usuario usuarioPreenchedor) {
        this.usuarioPreenchedor = usuarioPreenchedor;
    }

    public Usuario getUsuarioAvaliador() {
        return usuarioAvaliador;
    }

    public void setUsuarioAvaliador(Usuario usuarioAvaliador) {
        this.usuarioAvaliador = usuarioAvaliador;
    }


    @Override
    public String toString() {
        return "Imagem{" +
                "caminho='" + caminho + '\'' +
                '}';
    }
}
