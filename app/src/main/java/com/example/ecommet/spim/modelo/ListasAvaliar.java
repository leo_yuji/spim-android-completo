package com.example.ecommet.spim.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ecommet on 06/06/2017.
 */

public class ListasAvaliar {
    private List<Caracteristica> caracteristicas = new ArrayList<>();
    private List<Imagem> imagens = new ArrayList<>();

    public List<Caracteristica> getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(List<Caracteristica> caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public List<Imagem> getImagens() {
        return imagens;
    }

    public void setImagens(List<Imagem> imagens) {
        this.imagens = imagens;
    }
}
