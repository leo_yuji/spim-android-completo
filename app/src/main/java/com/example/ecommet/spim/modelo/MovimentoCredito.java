package com.example.ecommet.spim.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class MovimentoCredito implements Serializable {


	private Long id;

	private Usuario usuario;
	private BigDecimal valor;
	private BigDecimal ecoins;;
	private TipMovement tipoMovimento;
	private Date dataAcao = new Date();
	private boolean pendente = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getEcoins() {
		return ecoins;
	}

	public void setEcoins(BigDecimal ecoins) {
		this.ecoins = ecoins;
	}

	public TipMovement getTipoMovimento() {
		return tipoMovimento;
	}

	public void setTipoMovimento(TipMovement tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}

	public Date getDataAcao() {
		return dataAcao;
	}

	public void setDataAcao(Date dataAcao) {
		this.dataAcao = dataAcao;
	}

	public boolean isPendente() {
		return pendente;
	}

	public void setPendente(boolean pendentes) {
		this.pendente = pendente;
	}
}
