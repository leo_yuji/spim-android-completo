package com.example.ecommet.spim.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ecommet on 03/04/2017.
 */

public class Produto implements Serializable {

    private Long id;
    private String nome;
    private List<Imagem> imagens;
    private BigDecimal remuneracaoPorAtributo;
    private int progresso;
    private boolean pendente = false;
    private BigDecimal preco;
    private Date dataCadastro = new Date();
    private boolean ativo = true;
    private Categoria categoria;
    private List<Caracteristica> caracteristicas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public BigDecimal getRemuneracaoPorAtributo() {
        return remuneracaoPorAtributo;
    }

    public List<Imagem> getImagens() {

        return imagens;
    }

    public void setImagens(List<Imagem> imagens) {
        this.imagens = imagens;
    }

    public void setRemuneracaoPorAtributo(BigDecimal remuneracaoPorAtributo) {
        this.remuneracaoPorAtributo = remuneracaoPorAtributo;
    }

    public int getProgresso() {
        return progresso;
    }

    public void setProgresso(int progresso) {
        this.progresso = progresso;
    }

    public boolean isPendente() {
        return pendente;
    }

    // @FormParam("pendente")
    public void setPendente(boolean pendente) {
        this.pendente = pendente;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Caracteristica> getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(List<Caracteristica> caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    @Override
    public String toString() {
        return nome;
    }



}
