package com.example.ecommet.spim.modelo;

public enum TipMovement {
ENTRADA("ENTRADA"),
SAIDA("SAÍDA");

    private String moviment;

    private TipMovement(String moviment){
        this.moviment = moviment;
    }

    @Override public String toString(){
        return moviment;
    }
}
