package com.example.ecommet.spim.modelo;

/**
 * Created by Ecommet on 12/05/2017.
 */

public enum TipoRemuneracao {
    maior_remuneracao("MAIOR REMUNERAÇÃO"),
    menor_remuneracao("MENOR REMUNERAÇÃO");

    private String remuneracao;

    private TipoRemuneracao(String remuneracao){
        this.remuneracao = remuneracao;
    }

    @Override public String toString(){
        return remuneracao;
    }
}
