package com.example.ecommet.spim.modelo;

import java.util.Date;

/**
 * Created by Ecommet on 20/04/2017.
 */

public class TokenJWT {
    private Long id;
    private String apiToken;
    private Date apiTokenExpiracao = null;

    public TokenJWT(String apiToken, Date apiTokenExpiracao) {
        apiToken = apiToken;
        apiTokenExpiracao = apiTokenExpiracao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Date getApiTokenExpiracao() {
        return apiTokenExpiracao;
    }

    public void setApiTokenExpiracao(Date apiTokenExpiracao) {
        this.apiTokenExpiracao = apiTokenExpiracao;
    }
}
