package com.example.ecommet.spim.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

/**
 * Created by Ecommet on 07/04/2017.
 */

public class Notifica  extends Dialog{

    public Notifica(@NonNull Context context) {
        super(context);
    }

    public static void toastCurto(Context contexto, String mensagem) {
        Toast.makeText(contexto, mensagem, Toast.LENGTH_SHORT).show();
    }



    public static void DialogMensagem(Activity activity,String tituloDialog, String mensagem) {
        AlertDialog alerta;

        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        //define o titulo
        builder.setTitle(tituloDialog);
        //define a mensagem
        builder.setMessage(mensagem);
        //define um botão como positivo
        builder.setPositiveButton("OK", new OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
            }
        });

        //cria o AlertDialog
        alerta = builder.create();

        //Exibe
        alerta.show();
    }


}
