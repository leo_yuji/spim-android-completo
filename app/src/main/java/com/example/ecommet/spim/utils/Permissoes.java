package com.example.ecommet.spim.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tecnico_Tarde on 09/11/2016.
 */

public class Permissoes {

    public static boolean checarPermissoes(Activity activity, int requestCode, String... permissoes) {
        List<String> listaNegacoes = new ArrayList<>();

        for (String permissao : permissoes) {
            //exibe o estado atual da permissão solicitada
            Log.d("MORADOR", ContextCompat.checkSelfPermission(activity, permissao) + "");

            //solicitação da permissao
            if (ContextCompat.checkSelfPermission(activity, permissao) != PackageManager.PERMISSION_GRANTED) {
                listaNegacoes.add(permissao);
            }

        }

        if (listaNegacoes.isEmpty()) {
            return true;
        } else {
            String[] permissoesNegadas = new String[listaNegacoes.size()];
            listaNegacoes.toArray(permissoesNegadas);
            ActivityCompat.requestPermissions(activity, permissoesNegadas, requestCode);

            return false;
        }


    }

}
