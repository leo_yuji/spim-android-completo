package com.example.ecommet.spim.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Date;


/**
 * Created by sn1022208 on 02/03/2016.
 */
public class Prefs {
    public static String getNomeUser(Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        return preferences.getString("nome_usuario", "");
    }


    public static String[] getAcesso(Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        String[] dados = new String[2];
        dados[0] = preferences.getString("login", "");
        dados[1] = preferences.getString("senha", "");
        return dados;
    }

    public static String getToken(Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        String token = "";

        token= preferences.getString("token", "");

        return token;
    }

    public static void gravarLogin(String login, String senha, Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("login", login);
        editor.putString("senha", senha);
        editor.commit();
    }

    public static void limparLogin(Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("login", "");
        editor.putString("senha", "");
        editor.commit();
    }

    public static void gravarToken(String token, Date dataEx, Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", token);
        editor.putLong("hora_expiracao",new Long(dataEx.getDate()) );
        editor.commit();
    }

    public static void setConectado(Context contexto, boolean manter) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("manter_conectado", manter);
        editor.commit();
    }

    public static Boolean isConectado(Context contexto) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        return preferences.getBoolean("manter_conectado", false);
    }

    public static String getEndereco(Context contexto){
        // recupera o IP das preferências
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(contexto);
        String ip = preferences.getString("ip", "192.168.3.4:8080/SPIM");
        // monta a URL para salvar o usuário
        String strUrl = "http://" + ip;
        return strUrl;
    }


}
