package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.util.Log;

import com.example.ecommet.spim.modelo.Atributo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ecommet on 10/04/2017.
 */

public class AtributoService {

    private static final String URL = "/rest/atributo";

    public static List<Atributo> listAtributos(Context contexto) throws Exception {
        List<Atributo> atributoList = new ArrayList<Atributo>();
        String json = WebService.get(URL + "/listar", contexto);
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject atributoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Atributo atributo = gson.fromJson(atributoObject.toString(), Atributo.class);

            atributoList.add(atributo);
        }
        return atributoList;
    }

    public static List<Atributo> listarAtributosPrincipais(Context contexto) throws Exception {
        List<Atributo> atributoList = new ArrayList<Atributo>();
        String json = WebService.get(URL + "/listarTipo/0", contexto);
        Log.e("JSON LIST", json.toString());
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject atributoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Atributo atributo = gson.fromJson(atributoObject.toString(), Atributo.class);

            atributoList.add(atributo);
        }
        return atributoList;
    }


}
