package com.example.ecommet.spim.webservice;

import android.content.Context;

import com.example.ecommet.spim.modelo.Banco;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ecommet on 10/04/2017.
 */

public class BancoService {

    private static final String URL = "/rest/banco";

    public static List<Banco> listBancos(Context contexto) throws Exception {
        List<Banco> bancosList = new ArrayList<Banco>();
        String json = WebService.get(URL + "/listar", contexto);
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject bancoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Banco banco = gson.fromJson(bancoObject.toString(), Banco.class);

            bancosList.add(banco);
        }
        return bancosList;
    }

    public static List<Banco> listarBanco(Context contexto, Long id) throws Exception {

        String json = WebService.get(URL + "/" + id, contexto);

        List<Banco> bancosList = new ArrayList<>();

        Banco b = new Banco();

        try {

            JSONObject bancoObject = new JSONObject(json);
            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Banco banco = gson.fromJson(bancoObject.toString(), Banco.class);

            bancosList.add(banco);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return bancosList;
    }
}
