package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.util.Log;

import com.example.ecommet.spim.modelo.MovimentoCredito;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ecommet on 10/04/2017.
 */

public class HistoricoMovimentosService {

    private static final String URL = "/rest/historico";



    public static List<MovimentoCredito> listarMovimentosBuscar(Context contexto, int buscarMovimento) throws Exception {

        List<MovimentoCredito> movimentoCreditoList = new ArrayList<>();
        String json = WebService.get(URL + "/tipo/" + String.valueOf(buscarMovimento), contexto);
        Log.e("RETORNO HIST", json+"");
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject historicoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            MovimentoCredito movimentoCredito = gson.fromJson(historicoObject.toString(), MovimentoCredito.class);
            movimentoCreditoList.add(movimentoCredito);
        }
        return movimentoCreditoList;
    }


    public static String solicitarRetirada(Context contexto, MovimentoCredito movimentoCredito) throws Exception {

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
        String json = gson.toJson(movimentoCredito);
        return WebService.post(json, URL + "/saida/", contexto);
    }
}
