package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;


import com.example.ecommet.spim.modelo.Produto;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by Ecommet on 07/04/2017.
 */

public class PreencherProdutoService {
    private static final String URL = "/rest/produto";



    public static String salvarImagem(Context contexto, String imagem64, Produto produto) throws Exception {
        String imagemBase64 = "";
        if (imagem64.toString() != null) {
            Bitmap bmpImagem = MediaStore.Images.Media.getBitmap(contexto.getContentResolver(), Uri.fromFile(new File(imagem64)));
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmpImagem.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] imagem = stream.toByteArray();


            imagemBase64 = Base64.encodeToString(imagem, Base64.DEFAULT);
        }


        String json = imagemBase64;
        return WebService.put(json, URL + "/mobile/" + produto.getId(), contexto);
    }
}
