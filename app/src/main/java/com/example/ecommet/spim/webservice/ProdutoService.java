package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.util.Log;

import com.example.ecommet.spim.modelo.Caracteristica;
import com.example.ecommet.spim.modelo.ListasAvaliar;
import com.example.ecommet.spim.modelo.Produto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Tecnico_Tarde on 07/03/2016.
 */
public class ProdutoService {
    private static final String URL = "/rest/produto";
    private static final String URL_CARACTERISTICAS = "/rest/caracteristica";


    public static List<Produto> buscarProdutosNome(Context contexto, String nome) throws Exception {


        String json = WebService.get(URL + "/buscar/" + nome, contexto);

        List<Produto> produtosList = new ArrayList<>();


        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject produtoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Produto produto = gson.fromJson(produtoObject.toString(), Produto.class);

            produtosList.add(produto);
        }
        return produtosList;
    }

    public static List<Produto> listarProdutosPorBusca(Context contexto, int buscarProdutos) throws Exception {
        List<Produto> produtosList = new ArrayList<>();
        String json = WebService.get(URL + "/pendentes/" + buscarProdutos, contexto);
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject produtoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Produto produto = gson.fromJson(produtoObject.toString(), Produto.class);
            produtosList.add(produto);
        }
        return produtosList;
    }


    public static String avaliarCaracteristicas(Context contexto, Long idProduto, ListasAvaliar listas) throws Exception {
        Log.e("LIST CARACS", listas.getCaracteristicas().size()+"");

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();


        String json = gson.toJson(listas);


        return WebService.put(json, URL_CARACTERISTICAS + "/avaliar/" + idProduto, contexto);
    }

    public static String preencherProduto(Context contexto, Long idProduto, ListasAvaliar listas) throws Exception {


        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();


        String json = gson.toJson(listas);


        return WebService.put(json, URL_CARACTERISTICAS + "/preencherMobile/" + idProduto, contexto);
    }


    public static String avaliarImagens(Context contexto, Long idImagens) throws Exception {


        return WebService.put(URL + "/imagem/" + idImagens, contexto);
    }

    public static List<Caracteristica> buscarCaracsNaoAvaliadas(Context contexto, Long idProduto) throws Exception {
        List<Caracteristica> caracteristicaList = new ArrayList<>();
        String json = WebService.get(URL_CARACTERISTICAS + "/filtrar/pendentes/" + idProduto, contexto);
        JSONArray jsonArray = new JSONArray(json);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject produtoObject = jsonArray.getJSONObject(i);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            Caracteristica caracteristica = gson.fromJson(produtoObject.toString(), Caracteristica.class);
            caracteristicaList.add(caracteristica);
        }
        return caracteristicaList;
    }

}
