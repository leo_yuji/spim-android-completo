package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


import com.auth0.android.jwt.JWT;
import com.example.ecommet.spim.modelo.Colaborador;
import com.example.ecommet.spim.modelo.Conta;
import com.example.ecommet.spim.modelo.Usuario;
import com.example.ecommet.spim.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Date;


/**
 * Created by Administrador on 03/03/2016.
 */
public class UsuarioService {
    private static final String URL_COLABORADOR = "/rest/colaborador";
    private static final String URL_USUARIO = "/rest/usuario";
    private static final String URL_COLABORADOR_ADD_CONTA = "/rest/conta";

    public static String cadastrarUsuario(Context contexto, Colaborador colaborador) throws Exception {
        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();


        String json = gson.toJson(colaborador);
        return WebService.postUsuario(json, URL_COLABORADOR + "/mobile", contexto);
    }

    public static String alterarSenha(Context contexto, Usuario usuario, String senhaAntiga) throws Exception {


        JSONObject job = new JSONObject();
        job.put("id", usuario.getId());
        job.put("senha", usuario.getSenha());


        return WebService.put(job.toString(), URL_USUARIO + "/alterarSenha/" + senhaAntiga, contexto);
    }

    public static String alterarDados(Context contexto, Colaborador colaborador) throws Exception {

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
        String json = gson.toJson(colaborador);


        return WebService.put(json, URL_COLABORADOR + "/", contexto);
    }


    public static String criarConta(Context contexto, Conta conta) throws Exception {

        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
        String json = gson.toJson(conta);

        return WebService.post(json, URL_COLABORADOR_ADD_CONTA + "/", contexto);
    }

    public static String recuperarSenha(Context contexto, Usuario usuario) throws Exception {


        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
        String json = gson.toJson(usuario);
        return WebService.post(json, URL_USUARIO + "/recuperarSenha", contexto);
    }

    public static String alterarImagem(Context contexto, Long id, String imagemBase64) throws Exception {

        return WebService.put(imagemBase64, URL_COLABORADOR + "/mobile/" + id, contexto);
    }


    public static Colaborador logar(Context contexto, String email, String senha) throws Exception {

        Colaborador c = null;
        JSONObject job = new JSONObject();
        job.put("email", email);
        job.put("senha", senha);
        String retorno = WebService.post(job.toString(), URL_USUARIO + "/login", contexto);
        if (!retorno.isEmpty()) {
            job = new JSONObject(retorno);

            c = new Colaborador();
            c.setEmail(email);
            c.setSenha(senha);


            JWT jwtToken = new JWT(job.getString("token"));
            String token = job.getString("token");
            if (jwtToken.getClaim("tipoUsuario").asString().equals("Colaborador")) {
                Date dateTokenExpiracao = jwtToken.getExpiresAt();

                Prefs.gravarToken(token, dateTokenExpiracao, contexto);


                JSONObject jsonUsuario = job.getJSONObject("usuario");
                Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
                c = gson.fromJson(jsonUsuario.toString(), Colaborador.class);

                return c;
            }



        }
        return null;
    }

    public static Colaborador buscarColaborador(Context context) throws Exception {

        Colaborador c = null;
        String retorno = WebService.get(URL_COLABORADOR + "/", context);
        if (!retorno.isEmpty()) {
            Log.e("RETORNO COLAB BUSCAR", retorno);

            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
            c = gson.fromJson(retorno, Colaborador.class);


        }
        return c;
    }
}
