package com.example.ecommet.spim.webservice;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import com.example.ecommet.spim.R;
import com.example.ecommet.spim.utils.Prefs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Administrador on 03/03/2016.
 */
public class WebService {
    /**
     * Método para realizar a leitura de um InputStream e convertê-lo em String
     *
     * @param in InputStream com os dados a serem lidos
     * @return conteúdo convertido em String
     */
    public static String readStream(InputStream in) {
        // cria uma referência para BufferedReader
        BufferedReader reader = null;
        // cria um StringBuilder
        StringBuilder builder = new StringBuilder();
        try {
            // cria o BufferedReader passando o InputStream recebido
            // como parâmetro
            reader = new BufferedReader(new InputStreamReader(in));
            // cria uma linha nula
            String line = "null";
            // lê as linhas e acrescentar ao StringBuilder
            // até que não haja mais conteúdo nas linhas
            while ((line = reader.readLine()) != null) {
                builder.append(line + "\n");
            }
        } catch (Exception e) {
            Log.w("ERRO", e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    // tenta fechar o reader
                    reader.close();
                } catch (Exception e2) {
                    Log.w("ERRO", e2.getMessage());
                }
            }
        }
        // retorna a String lida
        return builder.toString();
    }


    /**
     * Método para ler os dados de um endereço HTTP
     *
     * @param urlAddres no formato http://
     * @return conteúdo lido
     */

    public static String makeRequest(String urlAddres) {
        // Cria uma referência para se conectar com uma URL Http
        HttpURLConnection con = null;
        // cria uma referência para uma URL
        URL url = null;
        // String de retorno
        String resposta = null;
        try {
            // instancia a url através do endereço recebido no método
            url = new URL(urlAddres);
            // abre a conexão e guarda na variável con
            con = (HttpURLConnection) url.openConnection();
            // invoca o método readStream para ler o conteúdo da página
            resposta = readStream(con.getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // fecha a conexão
            con.disconnect();
        }
        // retorna o texto lido
        return resposta;
    }

    public static String get(String URL, Context contexto) throws IOException {
        String retorno = null;

        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;
            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestMethod("GET");
                conexao.setRequestProperty("Authorization", Prefs.getToken(contexto));
                conexao.setConnectTimeout(15000);
                conexao.setReadTimeout(15000);
                conexao.connect();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
                    Log.e("ERRO", "ERRO AO REALIZAR O GET");
                    Log.e("STATUS GET", status+"");
                    in = conexao.getErrorStream();
                } else {
                    in = conexao.getInputStream();
                }
                retorno =  WebService.readStream(in);
                Log.w("RETORNO BUSCAR", retorno);
                in.close();

            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }

    public static String getApp(String URL, Context contexto) throws IOException {
        String retorno = null;

        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;
            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestMethod("GET");
                conexao.setConnectTimeout(15000);
                conexao.setReadTimeout(15000);
                conexao.connect();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
                    Log.e("ERRO", "ERRO AO REALIZAR O GET");
                    Log.e("STATUS GET", status+"");
                    in = conexao.getErrorStream();
                } else {
                    in = conexao.getInputStream();
                }
                retorno =  WebService.readStream(in);
                Log.w("RETORNO", retorno);
                in.close();

            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }

    public static String post(String json, String URL, Context contexto) throws Exception {
        String retorno;
        int Status;
        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;

            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestProperty("Content-Type", "application/json");
                conexao.setRequestProperty("Authorization", Prefs.getToken(contexto));
                conexao.setRequestMethod("POST");
                conexao.setDoInput(true);
                conexao.setDoOutput(true);
                conexao.connect();
                OutputStream out = conexao.getOutputStream();
                out.write(json.getBytes("UTF-8"));
                out.flush();
                out.close();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {

                    Log.e("ERRO", "ERRO AO REALIZAR O POST");
                    in = conexao.getErrorStream();
                    throw new Exception(status+"");



                } else {


                    in = conexao.getInputStream();

                }
                retorno =   WebService.readStream(in);
                Log.w("RETORNO", retorno );
                in.close();
            } catch (IOException e) {
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }

    public static String postUsuario(String json, String URL, Context contexto) throws Exception {
        String retorno;
        int Status;
        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;

            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestProperty("Content-Type", "application/json");
                conexao.setRequestMethod("POST");
                conexao.setDoInput(true);
                conexao.setDoOutput(true);
                conexao.connect();
                OutputStream out = conexao.getOutputStream();
                out.write(json.getBytes("UTF-8"));
                out.flush();
                out.close();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {

                    Log.e("ERRO", "ERRO AO REALIZAR O POST");
                    in = conexao.getErrorStream();
                    throw new Exception(status+"");



                } else {


                    in = conexao.getInputStream();

                }
                retorno =   WebService.readStream(in);
                Log.w("RETORNO", retorno );
                in.close();
            } catch (IOException e) {
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }


    public static String put(String URL, Context contexto) throws Exception {
        String retorno;
        int Status;
        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;

            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestProperty("Content-Type", "application/json");
                conexao.setRequestProperty("Authorization", Prefs.getToken(contexto));
                conexao.setRequestMethod("PUT");
                conexao.setDoInput(true);
                conexao.connect();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {

                    Log.e("ERRO", "ERRO AO REALIZAR O PUT");
                    in = conexao.getErrorStream();
                    throw new Exception(status+"");



                } else {


                    in = conexao.getInputStream();

                }
                retorno =   WebService.readStream(in);
                Log.w("RETORNO", retorno );
                in.close();
            } catch (IOException e) {
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }


    public static String put(String json, String URL, Context contexto) throws Exception {
        String retorno;
        int Status;
        if (isConexaoDisponivel(contexto)) {
            String strUrl = Prefs.getEndereco(contexto) + URL;
            // cria a URL baseado na String com o endereço completo do service
            java.net.URL url = new URL(strUrl);
            HttpURLConnection conexao = null;
            try {
                conexao = (HttpURLConnection) url.openConnection();
                conexao.setRequestProperty("Content-Type", "application/json");
                conexao.setRequestProperty("Authorization", Prefs.getToken(contexto));
                conexao.setRequestMethod("PUT");
                conexao.setDoInput(true);
                conexao.setDoOutput(true);
                conexao.connect();
                OutputStream out = conexao.getOutputStream();
                out.write(json.getBytes("UTF-8"));
                out.flush();
                out.close();
                InputStream in = null;
                int status = conexao.getResponseCode();
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
                    Log.e("ERRO STATUS PUT", status+"");
                    Log.e("ERRO", "ERRO AO REALIZAR O PUT");
                    Log.e("STATUS PUT", status+"");
                    in = conexao.getErrorStream();
                } else {
                    in = conexao.getInputStream();
                }
                retorno =   WebService.readStream(in);
                Log.w("RETORNO", retorno );
                in.close();
            } catch (IOException e) {
                throw e;
            } finally {
                if (conexao != null) {
                    conexao.disconnect();
                }
            }
        } else {
            return contexto.getString(R.string.sem_conexao);
        }
        return retorno;
    }


    public static boolean isConexaoDisponivel(Context context) {
        //return true;
        try {
            ConnectivityManager conectividade = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conectividade == null) {
                return false;
            } else {
                NetworkInfo info = conectividade.getActiveNetworkInfo();
                if (info.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

}
